﻿using FluentEmail.Core;
using FluentEmail.Smtp;
using OfficeOpenXml;
using SchoolScheduleItemManager.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Windows;

namespace SchoolScheduleItemManager
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            try
            {
                log.Info($"-------------------- {Assembly.GetExecutingAssembly().GetName().Name} (v{Assembly.GetExecutingAssembly().GetName().Version}) --------------------");
                MainWinView mainWin = new MainWinView();
                // set eplus licence 
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                mainWin.Show();
            }
            catch(Exception ex)
            {
                log.Error("Fatal exception cathed. Exiting app", ex);
                Environment.Exit(1);
            }
        }
    }
}
