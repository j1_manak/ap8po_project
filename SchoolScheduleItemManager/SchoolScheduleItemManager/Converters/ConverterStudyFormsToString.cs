﻿using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SchoolScheduleItemManager.Converters
{
    class ConverterStudyFormsToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ICollection<StudyForms> val)
            {
                var newColl = new ObservableCollection<string>();
                foreach (var item in val) newColl.Add(EnumExtensions.GetDisplayName(item));
                return newColl;
            }
            else if (value is StudyForms vall)
            {
                return EnumExtensions.GetDisplayName(vall);
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var values = Enum.GetValues(typeof(StudyForms));
            foreach (var val in values)
            {
                var st = EnumExtensions.GetDisplayName((StudyForms)val);
                if (st == (string)value) return (StudyForms)val;
            }
            return null;
        }
    }
}
