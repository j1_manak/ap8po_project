﻿using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SchoolScheduleItemManager.Converters
{
    public class ConverterFinishTypesToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is ICollection<FinishTypes> val)
            {
                var newColl = new ObservableCollection<string>();
                foreach (var item in val) newColl.Add(EnumExtensions.GetDisplayName(item));
                return newColl;
            }
            else if(value is FinishTypes vall)
            {
                return EnumExtensions.GetDisplayName(vall);
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var values = Enum.GetValues(typeof(FinishTypes));
            foreach (var val in values)
            {
                var st = EnumExtensions.GetDisplayName((FinishTypes)val);
                if (st == (string)value) return (FinishTypes)val;
            }
            return null;
        }
    }
}
