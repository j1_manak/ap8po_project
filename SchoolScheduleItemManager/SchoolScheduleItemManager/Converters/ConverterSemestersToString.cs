﻿using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows.Data;

namespace SchoolScheduleItemManager.Converters
{
    class ConverterSemestersToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ICollection<Semesters> val)
            {
                var newColl = new ObservableCollection<string>();
                foreach (var item in val) newColl.Add(EnumExtensions.GetDisplayName(item));
                return newColl;
            }
            else if (value is Semesters vall)
            {
                return EnumExtensions.GetDisplayName(vall);
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var values = Enum.GetValues(typeof(Semesters));
            foreach (var val in values)
            {
                var st = EnumExtensions.GetDisplayName((Semesters)val);
                if (st == (string)value) return (Semesters)val;
            }
            return null;
        }
    }
}
