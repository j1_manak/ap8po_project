﻿using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows.Data;

namespace SchoolScheduleItemManager.Converters
{
    public class ConverterLabelTypesToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ICollection<LabelTypes> val)
            {
                var newColl = new ObservableCollection<string>();
                foreach (var item in val) newColl.Add(EnumExtensions.GetDisplayName(item));
                return newColl;
            }
            else if (value is LabelTypes vall)
            {
                return EnumExtensions.GetDisplayName(vall);
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var values = Enum.GetValues(typeof(LabelTypes));
            foreach (var val in values)
            {
                var st = EnumExtensions.GetDisplayName((LabelTypes)val);
                if (st == (string)value) return (LabelTypes)val;
            }
            return null;
        }
    }
}
