﻿using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SchoolScheduleItemManager.Converters
{
    class ConverterLanguageToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is ICollection<Languages> vall)
            {
                var newColl = new ObservableCollection<string>();
                foreach (var item in vall) newColl.Add(EnumExtensions.GetDisplayName(item));
                return newColl;
            }
            if (value is Languages val)
            {
                return EnumExtensions.GetDisplayName(val);
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var values = Enum.GetValues(typeof(Languages));
            foreach (var val in values)
            {
                var st = EnumExtensions.GetDisplayName((Languages)val);
                if (st == (string)value) return (Languages)val;
            }
            return null;
        }
    }
}
