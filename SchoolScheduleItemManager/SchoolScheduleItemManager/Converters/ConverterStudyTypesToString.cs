﻿using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Windows.Data;

namespace SchoolScheduleItemManager.Converters
{
    public class ConverterStudyTypesToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ICollection<StudyTypes> val)
            {
                var newColl = new ObservableCollection<string>();
                foreach (var item in val) newColl.Add(EnumExtensions.GetDisplayName(item));
                return newColl;
            }
            else if (value is StudyTypes vall)
            {
                return EnumExtensions.GetDisplayName(vall);
            }

            if (culture is null)
            {
                throw new ArgumentNullException(nameof(culture));
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var values = Enum.GetValues(typeof(StudyTypes));
            foreach (var val in values)
            {
                var st = EnumExtensions.GetDisplayName((StudyTypes)val);
                if (st == (string)value) return (StudyTypes)val;
            }
            return null;
        }
    }
}
