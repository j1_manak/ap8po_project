﻿using SchoolScheduleItemManager.Properties;
using SchoolScheduleItemManager.Views;
using ModernWpf.Controls;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SchoolScheduleItemManager.Utils
{
    public enum Icons { Info, Error, Warning, Question }

    class Messages
    {
        public static async Task MsgWarning(string text)
        {
            ContentDialog dialog = new ContentDialog()
            {
                Title = Resources.Messages_Warning,
                Content = new ContentDialogContent(Icons.Warning, text),
                PrimaryButtonText = "OK",
                IsSecondaryButtonEnabled = false,
                DefaultButton = ContentDialogButton.Primary
            };

            await dialog.ShowAsync();
        }

        public static async Task MsgError(string text)
        {
            ContentDialog dialog = new ContentDialog()
            {
                Title = Resources.Messages_Error,
                Content = new ContentDialogContent(Icons.Error, text),
                PrimaryButtonText = "OK",
                IsSecondaryButtonEnabled = false,
                DefaultButton = ContentDialogButton.Primary
            };

            await dialog.ShowAsync();
        }

        public static async Task<bool> MsgYesNo(string text)
        {
            ContentDialog dialog = new ContentDialog()
            {
                Title = Resources.Messages_Question,
                Content = new ContentDialogContent(Icons.Question, text),
                PrimaryButtonText = "Ano",
                SecondaryButtonText = "Ne",
                DefaultButton = ContentDialogButton.Primary
            };

            var res = await dialog.ShowAsync();
            return res == ContentDialogResult.Primary;
        }

        public static async Task MsgInfo(string text)
        {
            ContentDialog dialog = new ContentDialog()
            {
                Title = Resources.Messages_Info,
                Content = new ContentDialogContent(Icons.Info, text),
                PrimaryButtonText = "OK",
                IsSecondaryButtonEnabled = false,
                DefaultButton = ContentDialogButton.Primary
            };

            await dialog.ShowAsync();
        }

        public static async Task<bool> MsgOkCancel(string text)
        {
            ContentDialog dialog = new ContentDialog()
            {
                Title = Resources.Messages_Question,
                Content = new ContentDialogContent(Icons.Question, text),
                PrimaryButtonText = "OK",
                SecondaryButtonText = "Zrušit",
                DefaultButton = ContentDialogButton.Primary
            };

            var res = await dialog.ShowAsync();
            return res == ContentDialogResult.Primary;
        }

        public static async Task<string> GetPassword(string emailAddress)
        {
            ContentDialog cd = new ContentDialog();
            var pwdDialogView = new PasswordDialogView(cd, emailAddress);
            cd.Content = pwdDialogView;
            await cd.ShowAsync();
            if (pwdDialogView.DialogRes) return pwdDialogView.EmailPassword;
            else return null;
        }

        public static ContentDialog GetWaitingDialog(string msg)
        {
            var cd = new ContentDialog();
            var dockPanel = new DockPanel();
            dockPanel.Children.Add(new ProgressRing
            {
                Width = 60,
                Height = 60,
                IsActive = true,
                Margin = new Thickness(10)
            });
            dockPanel.Children.Add(new TextBlock
            {
                Text = msg,
                Margin = new Thickness(10, 5, 10, 5),
                FontSize = 16,
                TextWrapping = TextWrapping.Wrap,
                VerticalAlignment = VerticalAlignment.Center
            });
            cd.Content = dockPanel;
            return cd;
        }
    }
}
