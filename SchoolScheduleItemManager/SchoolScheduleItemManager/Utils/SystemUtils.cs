﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace SchoolScheduleItemManager.Utils
{
    public static class SystemUtils
    {
        public static string UserTmpDirectory
        {
            get
            {
                string tmpDirectory = Path.GetTempPath() +
                                      System.Reflection.Assembly.GetExecutingAssembly().GetName().Name +
                                      Path.DirectorySeparatorChar;

                //pokud složka neexistuje, vytvořit
                DirectoryInfo dir = new DirectoryInfo(tmpDirectory);
                if (!dir.Exists)
                {
                    dir.Create();
                }

                return tmpDirectory;
            }
        }

        public static string UserLocalAppDirectory
        {
            get
            {
                string localAppDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + Path.DirectorySeparatorChar);

                //pokud složka neexistuje, vytvořit
                DirectoryInfo dir = new DirectoryInfo(localAppDirectory);
                if (!dir.Exists)
                {
                    dir.Create();
                }

                return localAppDirectory;
            }
        }

        public static ObservableCollection<T> ConvertListToOC<T>(List<T> coll)
        {
            var oc = new ObservableCollection<T>();
            foreach (var item in coll) oc.Add(item);
            return oc;
        }

        public static bool IsFileOccupied(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
                return false;
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        public static void RemoveAll<T>(this ICollection<T> collection, Func<T, bool> predicate)
        {
            T element;

            for (int i = 0; i < collection.Count; i++)
            {
                element = collection.ElementAt(i);
                if (predicate(element))
                {
                    collection.Remove(element);
                    i--;
                }
            }
        }
    }
}
