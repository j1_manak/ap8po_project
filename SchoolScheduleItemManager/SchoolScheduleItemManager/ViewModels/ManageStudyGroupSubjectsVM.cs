﻿using ModernWpf.Controls;
using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SchoolScheduleItemManager.ViewModels
{
    public class ManageStudyGroupSubjectsVM : BaseVM
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ManageStudyGroupSubjectsVM));
        private readonly ContentDialog _dialog;
        private readonly StudyGroup _studyGroup;

        public ManageStudyGroupSubjectsVM(ContentDialog dialog, StudyGroup studyGroup)
        {
            _dialog = dialog;
            _studyGroup = studyGroup;
            InitCommads();
            InitAddedSubjects();
            InitAvailableSubjects();
        }

        #region Properties

        public ObservableCollection<Subject> Subjects { get; set; }
        public Subject SelectedSubject { get; set; }
        public ObservableCollection<Subject> AddedSubjects { get; set; }
        public Subject SelectedAddedSubject { get; set; }

        #endregion

        #region CommandsHandlers

        public ICommand CommandAddSubject { get; set; }
        public ICommand CommandRemoveSubject { get; set; }
        public ICommand CommandClose { get; set; }
        public ICommand CommandSaveAndClose { get; set; }

        private void AddSubject()
        {
            try
            {
                if (SelectedSubject == null) return;

                AddedSubjects.Add(SelectedSubject);
                Subjects.Remove(SelectedSubject);
                SelectedSubject = null;
            }
            catch (Exception ex)
            {
                log.Error("Add subject failed!!!", ex);
            }
        }

        private void RemoveSubject()
        {
            try
            {
                if (SelectedAddedSubject == null) return;
                var subject = new Subject(Manager.Instance.ApplicationData.Subjects.First(x => x.Id == SelectedAddedSubject.Id));
                AddedSubjects.Remove(SelectedAddedSubject);
                Subjects.Add(subject);
                SelectedAddedSubject = null;
            }
            catch (Exception ex)
            {
                log.Error("Remove subject from study group failed!!!", ex);
            }
        }

        private void Close() => _dialog.Hide();

        private void SaveAndClose()
        {
            try
            {
                foreach (var item in AddedSubjects) _studyGroup.SubjectIds = GetSubjectIds();
                Manager.Instance.SerializeAppData();
                _dialog.Hide();
            }
            catch (Exception ex)
            {
                log.Error("Save and close failed!!!", ex);
            }
        }

        #endregion

        #region PrivateMethods

        private void InitCommads()
        {
            CommandAddSubject = new CommandGeneric(AddSubject);
            CommandClose = new CommandGeneric(Close);
            CommandRemoveSubject = new CommandGeneric(RemoveSubject);
            CommandSaveAndClose = new CommandGeneric(SaveAndClose);
        }

        private void InitAddedSubjects()
        {
            AddedSubjects = new ObservableCollection<Subject>();
            if (!string.IsNullOrWhiteSpace(_studyGroup.SubjectIds))
            {
                var alreadyAdded = _studyGroup.SubjectIds.Split(',');
                foreach (var item in alreadyAdded)
                {
                    var sub = Manager.Instance.ApplicationData.Subjects.FirstOrDefault(x => x.Id.ToString() == item);
                    if (sub != null) AddedSubjects.Add(new Subject(sub));
                    else
                    {
                        _studyGroup.RemoveSubject(item);
                        Manager.Instance.SerializeAppData();
                    }
                }
            }
        }

        private void InitAvailableSubjects()
        {
            Subjects = new ObservableCollection<Subject>();
            foreach (var item in Manager.Instance.ApplicationData.Subjects) Subjects.Add(new Subject(item));
            foreach(var item in AddedSubjects)
            {
                var sub = Subjects.FirstOrDefault(x => x.Id == item.Id);
                if (sub != null) Subjects.Remove(sub);
            }
        }

        private string GetSubjectIds()
        {
            var sb = new StringBuilder();
            var cnt = 1;
            foreach(var item in AddedSubjects)
            {
                if (cnt == 1) sb.Append(item.Id);
                else sb.Append($",{item.Id}");
                cnt++;
            }
            return sb.ToString();
        }

        #endregion
    }
}
