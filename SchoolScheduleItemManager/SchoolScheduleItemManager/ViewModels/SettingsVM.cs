﻿using Microsoft.Win32;
using ModernWpf.Controls;
using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.Properties;
using SchoolScheduleItemManager.Utils;
using SchoolScheduleItemManager.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SchoolScheduleItemManager.ViewModels
{
    public class SettingsVM : BaseVM
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(SettingsVM));
        private readonly MainWinVM _vmMainWin;

        public SettingsVM(MainWinVM vmMainWin)
        {
            _vmMainWin = vmMainWin;
            InitCommads();
            InitProperties();
        }

        #region Properties

        private bool _saveToDabase;
        public bool SaveToDabase 
        { 
            get => _saveToDabase; 
            set 
            {
                _saveToDabase = value;
                OnPropertyChanged(nameof(SaveToDabase));
                if (value)
                {
                    Manager.Instance.Settings.SelectedStorageType = StorageTypes.Database;
                    Manager.Instance.SwitchDataStorage();
                    _vmMainWin.ReloadUserControls();
                }
            }
        }

        private bool _saveToXml;
        public bool SaveToXml 
        { 
            get => _saveToXml; 
            set 
            { 
                _saveToXml = value; 
                OnPropertyChanged(nameof(SaveToXml));
                if (value)
                {
                    Manager.Instance.Settings.SelectedStorageType = StorageTypes.Xml;
                    Manager.Instance.SwitchDataStorage();
                    _vmMainWin.ReloadUserControls();
                }
            } 
        }

        private string _smtpServerAddress;
        public string SmtpServerAddress { get => _smtpServerAddress; set { _smtpServerAddress = value; OnPropertyChanged(nameof(SmtpServerAddress)); } }

        private int _smtpServerPort;
        public int SmtpServerPort { get => _smtpServerPort; set { _smtpServerPort = value; OnPropertyChanged(nameof(SmtpServerPort)); } }

        private string _emailAddress;
        public string EmailAddress { get => _emailAddress; set { _emailAddress = value; OnPropertyChanged(nameof(EmailAddress)); } }

        #endregion

        #region Commands

        //public ICommand CommandImportAppData { get; set; }
        //public ICommand CommandExportAppData { get; set; }
        public ICommand CommandSave { get; set; }
        public ICommand CommandOpenDataDir { get; set; }

        /*private async void ImportAppData()
        {
            try
            {
                var dialog = new OpenFileDialog()
                {
                    Title = "Importovat aplikační data",
                    Filter = "Xml soubor|*.xml|Databázový soubor|*.mdf",
                    Multiselect = false,
                    CheckPathExists = true,
                    CheckFileExists = true
                };
                var res = dialog.ShowDialog(Application.Current.MainWindow);
                if(res.HasValue && res.Value)
                {
                    Manager.Instance.ImportAppData(dialog.FileName);
                    await Messages.MsgInfo("Data byla úspěšně improtována.");
                }
            }
            catch (Exception ex)
            {
                log.Error("Import app data failed!!!", ex);
                await Messages.MsgError($"Při improtování aplikačních dat nastala chyba - {ex.Message}");
            }
        }

        private async void ExportAppData()
        {
            try
            {
                ContentDialog dialog = new ContentDialog()
                {
                    Title = Resources.Messages_Question,
                    Content = new ContentDialogContent(Icons.Question, "Chcete exportovat aplikační data ve formátu xml nebo databázovém souboru?"),
                    PrimaryButtonText = "Xml",
                    SecondaryButtonText = "Databáze",
                    DefaultButton = ContentDialogButton.Primary
                };

                var res = await dialog.ShowAsync();
                if (res == ContentDialogResult.None) return;

                var saveDialog = new SaveFileDialog()
                {
                    Title = "Vyberte místo uložení aplikačních dat",
                    Filter = res == ContentDialogResult.Primary ? "Xml soubor|*.xml" : "Soubor databáze|*.mdf",
                    AddExtension = true,
                    CheckPathExists = true,
                    OverwritePrompt = true,
                };
                var sdRes = saveDialog.ShowDialog(Application.Current.MainWindow);
                if(sdRes.HasValue && sdRes.Value)
                {
                    Manager.Instance.ExportAppData(res == ContentDialogResult.Primary ? StorageTypes.Xml : StorageTypes.Database, saveDialog.FileName);
                    await Messages.MsgInfo($"Aplikační data byla úspešně exportována do souboru {saveDialog.FileName}");
                }
            }
            catch (Exception ex)
            {
                log.Error("Export app data failed!!!", ex);
                await Messages.MsgError($"Při exportování aplikačních nastala chyba - {ex.Message}");
            }
        }*/

        private async void Save()
        {
            try
            {
                Manager.Instance.Settings.EmailAddress = EmailAddress;
                Manager.Instance.Settings.SmtpServerAddress = SmtpServerAddress;
                Manager.Instance.Settings.SmtpServerPort = SmtpServerPort;

                Manager.Instance.SerializeSettings();
                await Messages.MsgInfo("Nastavení bylo úspěšně uloženo");
            }
            catch (Exception ex)
            {
                log.Error("Save settings failed!!!", ex);
                await Messages.MsgError($"Při ukládání nastavení nastala chyba - {ex.Message}");
            }
        }

        private async void OpenDatadir()
        {
            try
            {
                Process.Start(SystemUtils.UserLocalAppDirectory);
            }
            catch (Exception ex)
            {
                log.Error("Oper data dir failed!!!", ex);
                await Messages.MsgError($"Při zobrazování adresáře umístění aplikačních dat nastala chyba - {ex.Message}");
            }
        }

        #endregion

        #region PrivateMethods

        private void InitCommads()
        {
            /*CommandImportAppData = new CommandGeneric(ImportAppData);
            CommandExportAppData = new CommandGeneric(ExportAppData);*/
            CommandSave = new CommandGeneric(Save);
            CommandOpenDataDir = new CommandGeneric(OpenDatadir);
        }

        private void InitProperties()
        {
            if (Manager.Instance.Settings.SelectedStorageType == StorageTypes.Xml) SaveToXml = true;
            else SaveToDabase = true;

            EmailAddress = Manager.Instance.Settings.EmailAddress;
            SmtpServerPort = Manager.Instance.Settings.SmtpServerPort;
            SmtpServerAddress = Manager.Instance.Settings.SmtpServerAddress;
        }

        #endregion
    }
}
