﻿using ModernWpf.Controls;
using SchoolScheduleItemManager.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace SchoolScheduleItemManager.ViewModels
{
    public class PasswordDialogVM : BaseVM
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(PasswordDialogVM));
        private readonly ContentDialog _cd;
        private readonly PasswordDialogView _passwordDialogView;

        public PasswordDialogVM(ContentDialog cd, string emailAddress, PasswordDialogView passwordDialogView)
        {
            _cd = cd;
            _passwordDialogView = passwordDialogView;
            EmailAddressText = $"Zadejte heslo od emailové adresy {emailAddress}";

            CommandOk = new CommandGeneric(Ok);
            CommandCancel = new CommandGeneric(Cancel);
        }

        #region Properties

        public string EmailAddressText { get; set; }
        public string Password { get; set; }

        #endregion

        #region Commands

        public ICommand CommandOk { get; set; }
        public ICommand CommandCancel { get; set; }

        private async void Ok()
        {
            try
            {
                if(string.IsNullOrWhiteSpace(Password))
                {
                    Flyout flyout = new Flyout();
                    var tb = new TextBlock();
                    tb.Text = "Není vyplněno heslo";
                    tb.Margin = new System.Windows.Thickness(10, 5, 10, 5);
                    tb.FontSize = 14;
                    flyout.Content = tb;
                    flyout.Placement = ModernWpf.Controls.Primitives.FlyoutPlacementMode.Bottom;
                    flyout.AreOpenCloseAnimationsEnabled = true;
                    flyout.ShowAt(_passwordDialogView.tbPwd);
                    return;
                }

                _passwordDialogView.DialogRes = true;
                _cd.Hide();
            }
            catch (Exception ex)
            {
                log.Error("Set password failed!!!", ex);
            }
        }

        private void Cancel()
        {
            _passwordDialogView.DataContext = false;
            _cd.Hide();
        }

        #endregion
    }
}
