﻿using FluentEmail.Core;
using FluentEmail.Core.Models;
using FluentEmail.Smtp;
using Microsoft.Win32;
using OfficeOpenXml;
using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SchoolScheduleItemManager.ViewModels
{
    public class EmployeeDetailVM : BaseVM
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(EmployeeDetailVM));
        private readonly MainWinVM _vmMainWin;
        private readonly Employee _selectedEmployee;

        public EmployeeDetailVM(MainWinVM vmMainWin, Employee selectedEmployee)
        {
            _vmMainWin = vmMainWin;
            _selectedEmployee = selectedEmployee;
            InitCommands();
            InitProperties();
        }

        #region Properties

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string WorkEmail { get; set; }
        public string PrivateEmail { get; set; }
        public double WorkPointNoEnglish { get; set; }
        public double WorkPointsWithEnglish { get; set; }
        public string Contract { get; set; }

        public ObservableCollection<WorkLabelModel> WorkLabelsColl { get; set; }

        private WorkLabelModel _selectedWorkLabel;
        public WorkLabelModel SelectedWorkLabelModel { get => _selectedWorkLabel; set { _selectedWorkLabel = value; OnPropertyChanged(nameof(SelectedWorkLabelModel)); } }

        #endregion

        #region Commands

        public ICommand CommandRemoveWorkLabel { get; set; }
        public ICommand CommandGoBack { get; set; }
        public ICommand CommandCreateWorkLabelsAndSave { get; set; }
        public ICommand CommandCreateWorkLabelsAndSendEmail { get; set; }

        private async void RemoveWorkLabel()
        {
            try
            {
                if (SelectedWorkLabelModel == null) return;

                SelectedWorkLabelModel.Employee = null;
                var label = Manager.Instance.ApplicationData.WorkLabels.FirstOrDefault(x => SelectedWorkLabelModel.Id == x.Id);
                if(label != null)
                {
                    label.EmployeeId = null;
                    Manager.Instance.SerializeAppData();
                }
                WorkLabelsColl.Remove(SelectedWorkLabelModel);
            }
            catch (Exception ex)
            {
                await Messages.MsgError($"Při odstraňování pracovního šítku zaměstnanci nastala chyba - {ex.Message}");
                log.Error("Remove work label in employee detail failed!!!", ex);
            }
        }

        private async void GoBack()
        {
            try
            {
                _vmMainWin.GoUcEmployees();
            }
            catch (Exception ex)
            {
                await Messages.MsgError($"Při návratu zpět na seznam zaměstnanců nastala chyba - {ex.Message}");
                log.Error("Return back to employee list from employee detail failed!!!", ex);
            }
        }

        private async void CreateWorkLabelsAndSave()
        {
            try
            {
                if (WorkLabelsColl == null || WorkLabelsColl?.Count <= 0)
                {
                    await Messages.MsgInfo("Zaměstnanec nemá přiřazeny žádné pracovní štítky. Xlsx soubor nebude vytvořen.");
                    return;
                }
                var xls = await CreateXlsFile();
                if (xls == null)
                {
                    await Messages.MsgWarning("Excel soubor se nepodařilo vytvořit");
                    return;
                }
                var sd = new SaveFileDialog
                {
                    Title = "Vyberte umístění pro souboru s pracovními štítky",
                    Filter = "Sešit excelu|*.xlsx|Sešit Excelu 97-2003|*.xls|Binární sešit Excelu|*.xlsb",
                    FileName = $"PracovniStitky_{_selectedEmployee.FirstName}-{_selectedEmployee.LastName}",
                    CheckPathExists = true,
                    OverwritePrompt = true
                };
                var dr = sd.ShowDialog(Application.Current.MainWindow);
                if (dr.HasValue && dr.Value)
                {
                    File.WriteAllBytes(sd.FileName, xls);
                    await Messages.MsgInfo($"Soubor byl úspěšně vytvořen a uložen do {sd.FileName}");
                }
            }
            catch (Exception ex)
            {
                await Messages.MsgError($"Při exportu pracovních štítků nebo při ukládání nastala chyba - {ex.Message}");
                log.Error("Export work labels and save into file failed!!!", ex);
            }
        }

        private async void CreateWorkLabelsAndSendEmail()
        {
            var wd = Messages.GetWaitingDialog("Probíhá odesílání emailu...");
            try
            {
                if (WorkLabelsColl == null || WorkLabelsColl?.Count <= 0)
                {
                    await Messages.MsgInfo("Zaměstnanec nemá přiřazeny žádné pracovní štítky. Xlsx soubor nebude vytvořen.");
                    return;
                }
                if (string.IsNullOrWhiteSpace(Manager.Instance.Settings.SmtpServerAddress) || string.IsNullOrWhiteSpace(Manager.Instance.Settings.EmailAddress) ||
                    Manager.Instance.Settings.SmtpServerPort == 0)
                {
                    await Messages.MsgWarning("Nezle odeslat pracovní štítky emailem, protože nejsou nastaveny všechny povinné položky pro odeslání emailů. " +
                        "Přejděte prosím do nastavení a nastavte zde adresu a port SMTP serveru a vaši emailouvou adresu.");
                    return;
                }

                var xls = await CreateXlsFile();
                if(xls == null)
                {
                    await Messages.MsgWarning("Excel soubor se nepodařilo vytvořit");
                    return;
                }

                var emailPwd = await Messages.GetPassword(Manager.Instance.Settings.EmailAddress);
                if(string.IsNullOrWhiteSpace(emailPwd))
                {
                    await Messages.MsgWarning("Nelze odeslat email, protože nebylo zadáno žádné heslo od emailu");
                    return;
                }

                var smtpClient = new SmtpClient()
                {
                    Host = Manager.Instance.Settings.SmtpServerAddress,
                    Credentials = new System.Net.NetworkCredential(Manager.Instance.Settings.EmailAddress, emailPwd),
                    Port = Manager.Instance.Settings.SmtpServerPort,
                };
                Email.DefaultSender = new SmtpSender(smtpClient);


                using (var ms = new MemoryStream(xls))
                {
                    var attachment = new FluentEmail.Core.Models.Attachment
                    {
                        Filename = $"PracovniStitky_{_selectedEmployee.FirstName}-{_selectedEmployee.LastName}.xlsx",
                        Data = ms
                    };

                    wd.ShowAsync();
                    var email = await Email
                        .From(Manager.Instance.Settings.EmailAddress)
                        .To(_selectedEmployee.WorkEmail)
                        .Subject("Seznam pracovních štítků")
                        .Body("V přiloze máte seznam Vašich pracovních šítků ve formátu sešitu excel")
                        .Attach(attachment)
                        .SendAsync();
                    wd.Hide();
                    if (email.Successful) await Messages.MsgInfo($"Soubor s pracovními štítky byl úspěšně vytvořen a odeslán emailem zaměnstanci");
                    else
                    {
                        var sb = new StringBuilder();
                        sb.AppendLine("Při odesílání emailu zaměstnanci nastala chyba");
                        if (email.ErrorMessages != null)
                        {
                            foreach (var error in email.ErrorMessages)
                                sb.AppendLine($"\t- {error}");
                        }
                        await Messages.MsgError(sb.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                wd.Hide();
                await Messages.MsgError($"Při exportu pracovních štítků nebo při odesílání emailu nastala chyba - {ex.Message}");
                log.Error("Export work labels and send by mail failed!!!", ex);
            }
        }

        #endregion

        #region PrivateMethods

        private void InitCommands()
        {
            CommandRemoveWorkLabel = new CommandGeneric(RemoveWorkLabel);
            CommandGoBack = new CommandGeneric(GoBack);
            CommandCreateWorkLabelsAndSave = new CommandGeneric(CreateWorkLabelsAndSave);
            CommandCreateWorkLabelsAndSendEmail = new CommandGeneric(CreateWorkLabelsAndSendEmail);
        }

        private void InitProperties()
        {
            FirstName = _selectedEmployee.FirstName;
            LastName = _selectedEmployee.LastName;
            WorkEmail = _selectedEmployee.WorkEmail;
            PrivateEmail = _selectedEmployee.PrivateEmail;
            WorkPointNoEnglish = _selectedEmployee.WorkingHoursNoEnglish;
            WorkPointsWithEnglish = _selectedEmployee.WorkingHoursWithEnglish;
            var employeeLabels = Manager.Instance.ApplicationData.WorkLabels.Where(x => x.EmployeeId == _selectedEmployee.Id).ToList();
            WorkLabelsColl = new ObservableCollection<WorkLabelModel>();
            foreach (var label in employeeLabels)
            {
                var labelModel = new WorkLabelModel
                {
                    Language = label.Language,
                    LabelType = label.LabelType,
                    WeekCount = label.WeekCount,
                    StudentCount = label.StudentCount,
                    HoursLength = label.HoursLength,
                    LabelName = label.Name,
                    Id = label.Id
                };
                if (label.EmployeeId != null)
                    labelModel.Employee = Manager.Instance.ApplicationData.Employees.FirstOrDefault(x => x.Id == label.EmployeeId.Value);
                if (label.SubjectId != null)
                    labelModel.Subject = Manager.Instance.ApplicationData.Subjects.FirstOrDefault(x => x.Id == label.SubjectId.Value);
                if (label.StudyGroupId != null)
                    labelModel.StudyGroup = Manager.Instance.ApplicationData.StudyGroups.FirstOrDefault(x => x.Id == label.StudyGroupId.Value);
                WorkLabelsColl.Add(labelModel);
            }
            Contract = GetContract();
        }

        private string GetContract()
        {
            if (_selectedEmployee.Contract == ContractTypes.PhdStudent)
                return EnumExtensions.GetDisplayName(_selectedEmployee.Contract);
            else if (_selectedEmployee.Contract == ContractTypes.Full)
            {
                string contractValue = "";
                if (double.Parse(_selectedEmployee.ContractSize) == 1.0)
                    contractValue = "(plný)";
                else if (double.Parse(_selectedEmployee.ContractSize) == 0.5)
                    contractValue = "(poloviční)";
                else contractValue = $"({_selectedEmployee.ContractSize})";

                return $"{EnumExtensions.GetDisplayName(_selectedEmployee.Contract)} {contractValue}";
            }
            else return string.Empty;
        }

        private async Task<byte[]> CreateXlsFile()
        {
            var xsl = new ExcelPackage();
            var ws = xsl.Workbook.Worksheets.Add("Pracovní štítky");
            // table headers
            ws.Cells["A1"].Value = "Název štítku";
            ws.Column(1).Width = 55;
            ws.Cells["B1"].Value = "Název předmětu";
            ws.Column(2).Width = 35;
            ws.Cells["C1"].Value = "Studijní skupina";
            ws.Column(3).Width = 16;
            ws.Cells["D1"].Value = "Typ štítku";
            ws.Column(4).Width = 16;
            ws.Cells["E1"].Value = "Počet studentů";
            ws.Column(5).Width = 15;
            ws.Cells["F1"].Value = "Počet týdnů";
            ws.Column(6).Width = 11;
            ws.Cells["G1"].Value = "Délka hodiny";
            ws.Column(7).Width = 13;
            ws.Cells["H1"].Value = "Jazyk";
            ws.Column(8).Width = 10;

            var cnt = 2;
            // fill alues
            foreach(var label in WorkLabelsColl)
            {
                ws.Cells[$"A{cnt}"].Value = label.LabelName;
                ws.Cells[$"B{cnt}"].Value = label.Subject?.Name;
                ws.Cells[$"C{cnt}"].Value = label.StudyGroup?.ToString();
                ws.Cells[$"D{cnt}"].Value = EnumExtensions.GetDisplayName(label.LabelType);
                ws.Cells[$"E{cnt}"].Value = label.StudentCount.ToString();
                ws.Cells[$"F{cnt}"].Value = label.WeekCount.ToString();
                ws.Cells[$"G{cnt}"].Value = label.HoursLength.ToString();
                ws.Cells[$"H{cnt}"].Value = EnumExtensions.GetDisplayName(label.Language);
                cnt++;
            }

            return await xsl.GetAsByteArrayAsync();
        }

        #endregion
    }
}
