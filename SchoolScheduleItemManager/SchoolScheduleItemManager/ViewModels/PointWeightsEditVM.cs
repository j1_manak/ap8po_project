﻿using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SchoolScheduleItemManager.ViewModels
{
    public class PointWeightsEditVM : BaseVM
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(PointWeightsEditVM));
        private readonly MainWinVM _vmMainWin;

        public PointWeightsEditVM(MainWinVM vmMainWin)
        {
            _vmMainWin = vmMainWin;
            CommandSave = new CommandGeneric(Save);
            Weights = Manager.Instance.Points;
        }

        #region Properties

        private PointWeightsModel _weights;
        public PointWeightsModel Weights { get => _weights; set { _weights = value; OnPropertyChanged(nameof(Weights)); } }

        #endregion

        #region Commands

        public ICommand CommandSave { get; set; }

        private async void Save()
        {
            try
            {
                if(!Weights.IsValid())
                {
                    await Messages.MsgWarning("Vstupy nejsou validní. Zkontrolujte zda jsou všechny položky vyplněné a zda jsou vložené číselné hodnoty");
                    return;
                }

                Manager.Instance.Points = Weights;
                Manager.Instance.SerializePoints();
            }
            catch (Exception ex)
            {
                log.Error("Save point weights failed!!!", ex);
                await Messages.MsgError($"Při ukládání vah nastala chyba - {ex.Message}");
            }
        }

        #endregion
    }
}
