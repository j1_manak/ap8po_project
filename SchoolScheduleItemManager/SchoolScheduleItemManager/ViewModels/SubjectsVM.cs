﻿using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SchoolScheduleItemManager.ViewModels
{
    public class SubjectsVM : BaseVM
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly MainWinVM _vmMainWin;
        private List<int> _deleteIds = new List<int>();

        // TODO: zamyslet se nad principem upozornění na to že poměnil data ale nezvolil uložení
        public SubjectsVM(MainWinVM vmMainWin)
        {
            _vmMainWin = vmMainWin;
            InitProperties();
            InitCommands();
        }

        #region Properties

        public ObservableCollection<Subject> SubjectsColl { get; set; }

        private Subject _selectedSubject;
        public Subject SelectedSubject { get => _selectedSubject; set { _selectedSubject = value; OnPropertyChanged(nameof(SelectedSubject)); } }

        public ObservableCollection<FinishTypes> FinishTypes { get; set; }

        public ObservableCollection<Languages> Languages { get; set; }

        #endregion

        #region Commands

        public ICommand CommandAddSubject { get; set; }
        public ICommand CommandDeleteSubject { get; set; }
        public ICommand CommandSaveChanges { get; set; }

        private async void AddSubject()
        {
            try
            {
                if (SubjectsColl == null) SubjectsColl = new ObservableCollection<Subject>();

                if(SubjectsColl.Count > 0)
                {
                    if (CheckLastItemFilled())
                    {
                        SubjectsColl.Add(new Subject() { Id = Manager.Instance.Settings.LastSubjectId });
                        Manager.Instance.IncrementSubjetId();
                    }
                    else await Messages.MsgWarning("Nemáte kompletně vyplněn předmět na posledním řádku. Pro přidání nového řádku je potřeba jej kompletně vyplnit nabo smazat");
                }
                else SubjectsColl.Add(new Subject());
            }
            catch (Exception ex)
            {
                log.Error("Add new subject failed!!!", ex);
                await Messages.MsgError($"Při přidávání předmětu nastala chyba - {ex.Message}");
            }
        }

        private async void DeleteSubject()
        {
            try
            {
                if (SelectedSubject == null) return;

                if (!await Messages.MsgYesNo($"Opravdu chcete smazat předmět {SelectedSubject.Shortcut} ({SelectedSubject.Name})")) return;

                if (SubjectsColl.Contains(SelectedSubject))
                {
                    _deleteIds.Add(SelectedSubject.Id);
                    SubjectsColl.Remove(SelectedSubject);
                }
            }
            catch (Exception ex)
            {
                log.Error("Delete subject failed!!!", ex);
                await Messages.MsgError($"Při odstraňování jednoho předmětu nastala chyba - {ex.Message}");
            }
        }

        private async void SaveChanges()
        {
            try
            {
                if (CheckLastItemFilled())
                {
                    if(_deleteIds.Count > 0)
                    {
                        RemoveDeletedSubjectsFromStudyGroups();
                        _deleteIds.Clear();
                    }
                    Manager.Instance.ApplicationData.Subjects = SubjectsColl;
                    Manager.Instance.SerializeAppData();
                    await Messages.MsgInfo("Změny úspěšně uloženy");
                }
                else await Messages.MsgWarning("Před uložením změn je potřeba kompletně vyplnit poslední předmět v tabulce nebo jej smazat");
            }
            catch (Exception ex)
            {
                log.Error("Save changes failed!!!", ex);
                await Messages.MsgError($"Při ukládání úprav v seznamu předmětů nastala chyba - {ex.Message}");
            }
        }

        #endregion

        #region PrivateMethods

        private void InitCommands()
        {
            CommandAddSubject = new CommandGeneric(AddSubject);
            CommandDeleteSubject = new CommandGeneric(DeleteSubject);
            CommandSaveChanges = new CommandGeneric(SaveChanges);
        }

        private void InitProperties()
        {
            FinishTypes = new ObservableCollection<FinishTypes>
            {
                 Models.FinishTypes.ClassifiedCredit,
                 Models.FinishTypes.Credit,
                 Models.FinishTypes.Exam
            };
            Languages = new ObservableCollection<Languages>
            {
                Models.Languages.Czech,
                Models.Languages.English
            };
            SubjectsColl = new ObservableCollection<Subject>();
            foreach(var subject in Manager.Instance.ApplicationData.Subjects) 
                SubjectsColl.Add(new Subject(subject));
        }

        private bool CheckLastItemFilled()
        {
            if (SubjectsColl.Count > 0)
            {
                var last = SubjectsColl.Last();
                if (!string.IsNullOrWhiteSpace(last.Shortcut) && !string.IsNullOrWhiteSpace(last.Name)) return true;
                else return false;
            }
            return true;
        }

        private void RemoveDeletedSubjectsFromStudyGroups()
        {
            foreach (var studyGroup in Manager.Instance.ApplicationData.StudyGroups)
            {
                foreach (var id in _deleteIds) studyGroup.RemoveSubject(id.ToString());
            }
            if(Manager.Instance.ApplicationData.WorkLabels != null && Manager.Instance.ApplicationData.WorkLabels.Count > 0)
            {
                foreach(var id in _deleteIds)
                {
                    var labelsToDelete = Manager.Instance.ApplicationData.WorkLabels.Where(x => x.SubjectId == id).ToList();
                    if(labelsToDelete != null && labelsToDelete.Count > 0)
                    {
                        foreach(var label in labelsToDelete)
                        {
                            Manager.Instance.ApplicationData.WorkLabels.Remove(label);
                        }
                    }
                }
            }
        }

        #endregion
    }
}
