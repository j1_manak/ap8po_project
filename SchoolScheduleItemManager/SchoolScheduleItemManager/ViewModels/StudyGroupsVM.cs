﻿using ModernWpf.Controls;
using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.Utils;
using SchoolScheduleItemManager.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SchoolScheduleItemManager.ViewModels
{
    public class StudyGroupsVM : BaseVM
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(StudyGroupsVM));
        private readonly MainWinVM _mainWinVM;
        private List<int> _deletedIds = new List<int>();

        public StudyGroupsVM(MainWinVM mainWinVM)
        {
            _mainWinVM = mainWinVM;
            InitCommands();
            InitProperties();
        }

        #region Properties

        public ObservableCollection<StudyGroup> StudyGroupsColl { get; set; }

        private StudyGroup _selectedStudyGroup;
        public StudyGroup SelectedStudyGroup { get => _selectedStudyGroup; set { _selectedStudyGroup = value; OnPropertyChanged(nameof(SelectedStudyGroup)); } }

        public ObservableCollection<Semesters> SemesterColl { get; set; }

        public ObservableCollection<StudyForms> StudyFormColl { get; set; }

        public ObservableCollection<StudyTypes> StudyTypeColl { get; set; }

        public ObservableCollection<Languages> LanguageColl { get; set; }

        #endregion

        #region Commands

        public ICommand CommandAddGroup { get; set; }
        public ICommand CommandSave { get; set; }
        public ICommand CommandDeleteStudyGroup { get; set; }
        public ICommand CommandManageGroupSubjects { get; set; }

        private async void AddGroup()
        {
            try
            {
                if (StudyFormColl == null) StudyFormColl = new ObservableCollection<StudyForms>();

                if (!ValidateLastInput())
                {
                    await Messages.MsgWarning("Před přidáním nové skupiny je potřeba vyplnit všechny údaje u posledně přidané skupiny nebo ji odstranit");
                    return;
                }

                StudyGroupsColl.Add(new StudyGroup() { Id = Manager.Instance.Settings.LastStudyGroupId });
                Manager.Instance.IncrementStudyGroupId();
            }
            catch (Exception ex)
            {
                log.Error("Add new study group failed!!!", ex);
                await Messages.MsgError($"Při přidávání nové skupiny nastala chyba - {ex.Message}");
            }
        }

        private async void Save()
        {
            try
            {
                var valRes = ValidateBeforeSave();
                if (!string.IsNullOrWhiteSpace(valRes))
                {
                    await Messages.MsgWarning($"Před uložením studijních skupinek je potřeba vyplnit všechny údaje u všech skupinek nebo je odstranit.\n Seznam řádků, které nejsou kompletně vyplněny - {valRes}");
                    return;
                }

                if(_deletedIds.Count > 0)
                {
                    DeleteWorkLabelsForDeletedStudyGroup();
                    _deletedIds.Clear();
                }

                // check student count for change
                foreach(var studyGroup in StudyGroupsColl)
                {
                    var savedStudyGroup = Manager.Instance.ApplicationData.StudyGroups.FirstOrDefault(x => x.Id == studyGroup.Id);
                    if (savedStudyGroup == null) continue;
                    if (savedStudyGroup.StudentCount == studyGroup.StudentCount) continue;

                    var subjectIds = studyGroup.SubjectIds?.Split(',');
                    if (subjectIds == null | subjectIds?.Length <= 0) continue;
                    foreach (var subjectId in subjectIds)
                    {
                        var subId = int.Parse(subjectId);
                        var subject = Manager.Instance.ApplicationData.Subjects.FirstOrDefault(x => subId == x.Id);
                        if (subject == null) continue;

                        var workLabels = Manager.Instance.ApplicationData.WorkLabels.Where(x => x.StudyGroupId == studyGroup.Id && x.SubjectId == subId).ToList();
                        if (workLabels == null || workLabels?.Count <= 0) continue;

                        foreach (var wl in workLabels)
                        {
                            if (wl.LabelType == LabelTypes.ClassifiedCredit || wl.LabelType == LabelTypes.Credit || wl.LabelType == LabelTypes.Exam || wl.LabelType == LabelTypes.Lecture)
                                wl.StudentCount = studyGroup.StudentCount;
                            else wl.StudentCount = 0;
                        }

                        workLabels = Manager.Instance.ApplicationData.WorkLabels.Where(x => x.StudyGroupId == studyGroup.Id && x.SubjectId == subId &&
                            x.LabelType != LabelTypes.ClassifiedCredit && x.LabelType != LabelTypes.Credit && x.LabelType != LabelTypes.Exam && x.LabelType != LabelTypes.Lecture).ToList();

                        double newLabelsCount = Math.Ceiling((double)studyGroup.StudentCount / (double)subject.StudentCount);
                        for (int j = 0; j < 2; j++)
                        {
                            List<WorkLabel> wls = null;
                            switch (j)
                            {
                                case 0: wls = workLabels.Where(x => x.LabelType == LabelTypes.Practice).ToList(); break;
                                case 1: wls = workLabels.Where(x => x.LabelType == LabelTypes.Seminar).ToList(); break;
                            }
                            if (wls == null || wls.Count <= 0) continue;
                            if (newLabelsCount == wls.Count)
                            {
                                for (int i = 0; i < studyGroup.StudentCount; i++)
                                {
                                    wls[(int)(i % newLabelsCount)].StudentCount++;
                                }
                            }
                            else if (newLabelsCount < wls.Count)
                            {
                                for (int i = 0; i < studyGroup.StudentCount; i++)
                                {
                                    wls[(int)(i % newLabelsCount)].StudentCount++;
                                }
                                var emptyWorkLabels = wls.Where(x => x.StudentCount == 0).ToList();
                                foreach (var emptyWorkLabel in emptyWorkLabels) Manager.Instance.ApplicationData.WorkLabels.Remove(emptyWorkLabel);
                            }
                            else // newLabelsCOunt > workLabels.Count
                            {
                                for (int i = wls.Count + 1; i <= newLabelsCount; i++)
                                {
                                    Manager.Instance.ApplicationData.WorkLabels.Add(new WorkLabel
                                    {
                                        Id = Manager.Instance.Settings.LastWorkLabelId,
                                        HoursLength = j == 0 ? subject.PracticeLength : subject.SeminarLength,
                                        LabelType = j == 0 ? LabelTypes.Practice : LabelTypes.Seminar,
                                        StudentCount = 0,
                                        StudyGroupId = studyGroup.Id,
                                        SubjectId = subject.Id,
                                        WeekCount = subject.WeekCount,
                                        Language = subject.Language
                                    });
                                    Manager.Instance.IncrementWorkLabelId();
                                }
                                var lblType = j == 0 ? LabelTypes.Practice : LabelTypes.Seminar;
                                wls = Manager.Instance.ApplicationData.WorkLabels.Where(x => x.SubjectId == subId && x.StudyGroupId == studyGroup.Id &&
                                            x.LabelType == lblType).ToList();
                                for (int i = 0; i < studyGroup.StudentCount; i++)
                                {
                                    wls[(int)(i % newLabelsCount)].StudentCount++;
                                }
                                var cnt = 1;
                                foreach (var wl in wls)
                                {
                                    wl.Name = $"{EnumExtensions.GetDisplayName(lblType)} {subject.Shortcut} {cnt}";
                                    cnt++;
                                }
                            }
                        }
                    }
                }

                Manager.Instance.ApplicationData.StudyGroups = StudyGroupsColl;
                Manager.Instance.SerializeAppData();
                Manager.Instance.SerializeSettings();
                await Messages.MsgInfo("Data byla uložena.");
                _mainWinVM.GenerateOrEditWorkLabels();
                InitProperties();
            }
            catch (Exception ex)
            {
                log.Error("Save study groups changes failed!!!", ex);
                await Messages.MsgError($"Při ukládání změn ve studijních skupinách nastala chyba - {ex.Message}");
            }
        }

        private async void DeleteStudyGroup()
        {
            try
            {
                if (SelectedStudyGroup == null) return;

                if (!await Messages.MsgYesNo($"Opravdu chcete odstranit skupinu se zkratkou {SelectedStudyGroup.Shortcut}?")) return;

                _deletedIds.Add(SelectedStudyGroup.Id);
                StudyGroupsColl.Remove(SelectedStudyGroup);
            }
            catch (Exception ex)
            {
                log.Error("Delete study group failed!!!", ex);
                await Messages.MsgError($"Při odstraňování studijní skupiny nastala chyba - {ex.Message}");
            }
        }

        private async void ManageGroupSubjects()
        {
            try
            {
                Manager.Instance.ApplicationData.StudyGroups = StudyGroupsColl;
                if(SelectedStudyGroup == null)
                {
                    await Messages.MsgInfo("Pro spravování předmětů studiní skupiny musí být vybrána nějaká skupina");
                    return;
                }
                if(Manager.Instance.ApplicationData.Subjects.Count <= 0)
                {
                    await Messages.MsgInfo("Pro přiřazování předmětů studijním skupinám musí existovat alespoň jeden předmět. Přidejte v sekci předměty nějaký předmět a " +
                        "budete moci pokračovat do sekce přiřazování předmětů ke studijním skupinám");
                    return;
                }

                ContentDialog cd = new ContentDialog();
                var dialogView = new ManageStudyGroupSubjectsView(cd, Manager.Instance.ApplicationData.StudyGroups.First(x => x.Id == SelectedStudyGroup.Id));
                cd.Content = dialogView;
                await cd.ShowAsync();
                Manager.Instance.SerializeAppData();
                InitProperties();
            }
            catch (Exception ex)
            {
                log.Error("Manage group subjects failed!!!", ex);
                await Messages.MsgError($"Při spravování předmětů skupiny nastala chyba - {ex.Message}");
            }
        }

        #endregion

        #region PrivateMethods

        private void InitCommands()
        {
            CommandAddGroup = new CommandGeneric(AddGroup);
            CommandSave = new CommandGeneric(Save);
            CommandDeleteStudyGroup = new CommandGeneric(DeleteStudyGroup);
            CommandManageGroupSubjects = new CommandGeneric(ManageGroupSubjects);
        }

        private void InitProperties()
        {
            SemesterColl = new ObservableCollection<Semesters>(Enum.GetValues(typeof(Semesters)) as IEnumerable<Semesters>);
            StudyFormColl = new ObservableCollection<StudyForms>(Enum.GetValues(typeof(StudyForms)) as IEnumerable<StudyForms>);
            StudyTypeColl = new ObservableCollection<StudyTypes>(Enum.GetValues(typeof(StudyTypes)) as IEnumerable<StudyTypes>);
            LanguageColl = new ObservableCollection<Languages>(Enum.GetValues(typeof(Languages)) as IEnumerable<Languages>);

            StudyGroupsColl = new ObservableCollection<StudyGroup>();
            OnPropertyChanged(nameof(StudyGroupsColl));

            foreach (var studyGroup in Manager.Instance.ApplicationData.StudyGroups)
                StudyGroupsColl.Add(new StudyGroup(studyGroup));
        }

        private bool ValidateLastInput(StudyGroup item = null)
        {
            if (StudyGroupsColl != null && StudyGroupsColl.Count > 0)
            {
                var last = item == null ? StudyGroupsColl.Last() : item;
                if (string.IsNullOrWhiteSpace(last.Shortcut) || last.StudyYear <= 0 || last.Semester == null || last.StudentCount <= 0 ||
                    last.StudyForm == null || last.StudyType == null || last.Language == null) return false;
                else return true;
            }
            else return true;
        }

        private string ValidateBeforeSave()
        {
            var sb = new StringBuilder();
            var cnt = 0;
            foreach(var item in StudyGroupsColl)
            {
                if(!ValidateLastInput(item))
                {
                    if (sb.Length == 0) sb.Append(cnt.ToString());
                    else sb.Append($", {cnt}");
                }
                ++cnt;
            }
            return sb.ToString();
        }

        private void DeleteWorkLabelsForDeletedStudyGroup()
        {
            var workLabels = Manager.Instance.ApplicationData.WorkLabels;
            if(workLabels != null & workLabels.Count > 0)
            {
                foreach(var id in _deletedIds) 
                {
                    SystemUtils.RemoveAll(workLabels, x => x.StudyGroupId == id);
                }
            }
        }

        #endregion
    }
}
