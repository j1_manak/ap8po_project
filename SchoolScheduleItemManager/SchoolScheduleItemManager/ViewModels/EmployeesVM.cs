﻿using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.Utils;
using SchoolScheduleItemManager.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SchoolScheduleItemManager.ViewModels
{
    public class EmployeesVM : BaseVM
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly MainWinVM _vmMainWin;
        private List<int> _deletedIds = new List<int>();

        public EmployeesVM(MainWinVM vmMainWin)
        {
            _vmMainWin = vmMainWin;
            InitCommands();
            InitProperties();
        }

        #region Properties

        public ObservableCollection<Employee> EmployeesColl { get; set; }

        private Employee _selectedEmployee;
        public Employee SelectedEmployee { get => _selectedEmployee; set { _selectedEmployee = value; OnPropertyChanged(nameof(SelectedEmployee)); } }

        public ObservableCollection<ContractTypes> ContractTypesColl { get; set; }

        #endregion

        #region Commands

        public ICommand CommandShowEmployeeDetail { get; set; }
        public ICommand CommandDeleteEmployee { get; set; }
        public ICommand CommandAddEmployee { get; set; }
        public ICommand CommandSaveChanges { get; set; }

        private async void ShowEmployeeDetail()
        {
            try
            {
                if (SelectedEmployee == null) return;

                EmployeeDetailView employeeDetailView = new EmployeeDetailView(_vmMainWin, SelectedEmployee);
                _vmMainWin.CurrentUserControl = employeeDetailView;
            }
            catch (Exception ex)
            {
                log.Error("Show employee failed!!!", ex);
                await Messages.MsgError($"Při zobrazování detailu zaměstnance nastala chyba - {ex.Message}");
            }
        }

        private async void DeleteEmployee()
        {
            try
            {
                if (SelectedEmployee == null) return;

                if (!await Messages.MsgYesNo($"Opravdu chcete odstranit uživatele {SelectedEmployee.Fullname}?")) return;

                _deletedIds.Add(SelectedEmployee.Id);
                EmployeesColl.Remove(SelectedEmployee);
            }
            catch (Exception ex)
            {
                log.Error("Delete selected employee failed!!!", ex);
                await Messages.MsgError($"Při odstraňování zvoleného zaměstnance nastala chyba - {ex.Message}");
            }
        }

        private async void AddEmployee()
        {
            try
            {
                if (EmployeesColl == null) EmployeesColl = new ObservableCollection<Employee>();

                if (CheckIfLastFilled())
                {
                    EmployeesColl.Add(new Employee() { Id = Manager.Instance.Settings.LastEmployeeId });
                    Manager.Instance.IncrementEmployeeId();
                }
                else await Messages.MsgWarning("Před přidáním dalšího zaměstnance je potřeba nejprve vyplnit všechny informace o posledně přidaném zaměstnanci.");
            }
            catch (Exception ex)
            {
                log.Error("Add employee failed!!!", ex);
                await Messages.MsgError($"Při přidávání nového uživatele nastala chyba - {ex.Message}");
            }
        }

        private async void SaveChanges()
        {
            try
            {
                if (CheckIfLastFilled())
                {
                    Manager.Instance.ApplicationData.Employees = EmployeesColl;
                    if (_deletedIds.Count > 0)
                    {
                        _vmMainWin.UnsetWorkLabelsForDeletedEmployees(_deletedIds);
                        _deletedIds.Clear();
                    }
                    Manager.Instance.SerializeAppData();
                    await Messages.MsgInfo("Změny byly uloženy");
                }
                else await Messages.MsgWarning("Před uložením změn v zaměstnancích je potřeba doplnit všechny informace o posledně přidaném zaměstnanci.");
            }
            catch (Exception ex)
            {
                log.Error("Save employee changes failed!!!", ex);
                await Messages.MsgError($"Při ukládání změn nastala chyba - {ex.Message}");
            }
        }

        #endregion

        #region PrivateMethods

        private void InitCommands()
        {
            CommandShowEmployeeDetail = new CommandGeneric(ShowEmployeeDetail);
            CommandDeleteEmployee = new CommandGeneric(DeleteEmployee);
            CommandAddEmployee = new CommandGeneric(AddEmployee);
            CommandSaveChanges = new CommandGeneric(SaveChanges);
        }

        private void InitProperties()
        {
            ContractTypesColl = new ObservableCollection<ContractTypes>() { ContractTypes.Full, ContractTypes.PhdStudent };
            EmployeesColl = new ObservableCollection<Employee>();
            foreach (var employee in Manager.Instance.ApplicationData.Employees)
                EmployeesColl.Add(employee);
        }

        private bool CheckIfLastFilled()
        {
            if (EmployeesColl.Count == 0) return true;

            var last = EmployeesColl.Last();
            if (!string.IsNullOrWhiteSpace(last.FirstName) && !string.IsNullOrWhiteSpace(last.LastName) && !string.IsNullOrWhiteSpace(last.WorkEmail) &&
                !string.IsNullOrWhiteSpace(last.PrivateEmail) && double.TryParse(last.ContractSize, out _)) return true;
            else return false;
        }

        #endregion
    }
}
