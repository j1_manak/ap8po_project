﻿using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SchoolScheduleItemManager.ViewModels
{
    public class WorkLabelVM : BaseVM
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(WorkLabelVM));
        private readonly MainWinVM _vmMainWin;

        public WorkLabelVM(MainWinVM vmMainWin)
        {
            _vmMainWin = vmMainWin;
            InitCommands();
            Employees = Manager.Instance.ApplicationData.Employees;
            GetWorkLabels();
        }

        #region Properties

        public ObservableCollection<WorkLabelModel> SettedWorkLabelsColl { get; set; }
        public ObservableCollection<WorkLabelModel> UnSettedWorkLabelsColl { get; set; }

        public WorkLabelModel SelectedSettedLabel { get => _selectedSettedLabel; set { _selectedSettedLabel = value; OnPropertyChanged(nameof(SelectedSettedLabel)); } }
        private WorkLabelModel _selectedSettedLabel;

        public WorkLabelModel SelectedUnSettedLabel { get => _selectedUnSettedLabel; set { _selectedUnSettedLabel = value; OnPropertyChanged(nameof(SelectedUnSettedLabel)); } }
        private WorkLabelModel _selectedUnSettedLabel;

        public ObservableCollection<Employee> Employees { get; set; }

        public Employee SelectedEmployee { get => _selectedEmployee; set { _selectedEmployee = value; OnPropertyChanged(nameof(SelectedEmployee)); } }
        private Employee _selectedEmployee;

        #endregion

        #region Commands

        public ICommand CommandSetEmployee { get; set; }
        public ICommand CommandRemoveEmployee { get; set; }
        public ICommand CommandSaveChanges { get; set; }

        private async void SetEmployee()
        {
            try
            {
                if (SelectedEmployee == null || SelectedUnSettedLabel == null) return;
                SelectedUnSettedLabel.Employee = SelectedEmployee;
                SettedWorkLabelsColl.Add(SelectedUnSettedLabel);
                UnSettedWorkLabelsColl.Remove(SelectedUnSettedLabel);
            }
            catch (Exception ex)
            {
                log.Error("Set employee failed!!!", ex);
                await Messages.MsgError($"Přiřazení zaměstnance k předmětu selhalo - {ex.Message}");
            }
        }

        private async void RemoveEmployee()
        {
            try
            {
                if (SelectedSettedLabel == null) return;
                SelectedSettedLabel.Employee = null;
                UnSettedWorkLabelsColl.Add(SelectedSettedLabel);
                SettedWorkLabelsColl.Remove(SelectedSettedLabel);
            }
            catch (Exception ex)
            {
                log.Error("Remove employee failed!!!", ex);
                await Messages.MsgError($"Odstranění přiřazení zaměstnance od předmětu selhalo - {ex.Message}");
            }
        }

        private async void SaveChanges()
        {
            try
            {
                foreach(var item in SettedWorkLabelsColl)
                {
                    var workLabel = Manager.Instance.ApplicationData.WorkLabels.FirstOrDefault(x => x.Id == item.Id);
                    if (workLabel == null) continue;
                    workLabel.EmployeeId = item.Employee.Id;
                }
                Manager.Instance.SerializeAppData();
                await Messages.MsgInfo("Změny byly uloženy");
            }
            catch (Exception ex)
            {
                log.Error("Save changes failed!!!", ex);
                await Messages.MsgError($"Při ukládání změň nastala chyba - {ex.Message}");
            }
        }

        #endregion

        #region PrivateMethods

        private void InitCommands()
        {
            CommandSetEmployee = new CommandGeneric(SetEmployee);
            CommandRemoveEmployee = new CommandGeneric(RemoveEmployee);
            CommandSaveChanges = new CommandGeneric(SaveChanges);
        }

        private void GetWorkLabels()
        {
            SettedWorkLabelsColl = new ObservableCollection<WorkLabelModel>();
            UnSettedWorkLabelsColl = new ObservableCollection<WorkLabelModel>();
            var workLabels = Manager.Instance.ApplicationData.WorkLabels;
            if (workLabels != null && workLabels.Count > 0)
            {
                foreach(var label in workLabels)
                {
                    var labelModel = new WorkLabelModel
                    {
                        Language = label.Language,
                        LabelType = label.LabelType,
                        WeekCount = label.WeekCount,
                        StudentCount = label.StudentCount,
                        HoursLength = label.HoursLength,
                        LabelName = label.Name,
                        Id = label.Id
                    };
                    if(label.EmployeeId != null)
                        labelModel.Employee = Manager.Instance.ApplicationData.Employees.FirstOrDefault(x => x.Id == label.EmployeeId.Value);
                    if (label.SubjectId != null)
                        labelModel.Subject = Manager.Instance.ApplicationData.Subjects.FirstOrDefault(x => x.Id == label.SubjectId.Value);
                    if (label.StudyGroupId != null)
                        labelModel.StudyGroup = Manager.Instance.ApplicationData.StudyGroups.FirstOrDefault(x => x.Id == label.StudyGroupId.Value);

                    if (labelModel.Employee != null) SettedWorkLabelsColl.Add(labelModel);
                    else UnSettedWorkLabelsColl.Add(labelModel);
                }
            }
        }

        #endregion
    }
}
