﻿using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.Utils;
using SchoolScheduleItemManager.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SchoolScheduleItemManager.ViewModels
{

    public class MainWinVM : BaseVM
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly Window _mainWin;

        public MainWinVM(Window mainWin)
        {
            _mainWin = mainWin;
            InitCommands();
            GoUcSubjects();
        }

        #region Properties

        #endregion

        #region Commands

        public ICommand CommandGoUcSubjects { get; set; }
        public ICommand CommandGoUcEmployees { get; set; }
        public ICommand CommandGoUcStudyGroups { get; set; }
        public ICommand CommandGoUcSettings { get; set; }
        public ICommand CommandGoUcWeights { get; set; }
        public ICommand CommandGoUcWorkLabels { get; set; }

        private async void GoUcSubjects()
        {
            try
            {
                CurrentUserControl = new SubjectsView(this);
            }
            catch (Exception ex)
            {
                log.Error("Go to uc subjects failed!!!", ex);
                await Messages.MsgError($"Při zobrazování obrazovky s předměty nastala chyba - {ex.Message}");
            }
        }

        internal async void GoUcEmployees()
        {
            try
            {
                CurrentUserControl = new EmployeesView(this);
            }
            catch (Exception ex)
            {
                log.Error("Go to uc Employees failed!!!", ex);
                await Messages.MsgError($"Při zobrazování obrazovky s předměty nastala chyba - {ex.Message}");
            }
        }

        private async void GoUcStudyGroups()
        {
            try
            {
                CurrentUserControl = new StudyGroupsView(this);
            }
            catch (Exception ex)
            {
                log.Error("Go to uc study groups failed!!!", ex);
                await Messages.MsgError($"Při zobrazování obrazovky se studijníma skupinama nastala chyba - {ex.Message}");
            }
        }

        private async void GoUcSettings()
        {
            try
            {
                CurrentUserControl = new SettingsView(this);
            }
            catch (Exception ex)
            {
                log.Error("Go to uc settings failed!!!", ex);
                await Messages.MsgError($"Při zobrazování obrazovky nastavení nastala chyba - {ex.Message}");
            }
        }

        private async void GoUcWeights()
        {
            try
            {
                CurrentUserControl = new PointWeightsEditView(this);
            }
            catch (Exception ex)
            {
                log.Error("Go to uc weights failed!!!", ex);
                await Messages.MsgError($"Při zobrazování obrazovky pro úpravu vah pracovních bodů nastala chyba - {ex.Message}");
            }
        }

        private async void GoUcWorkLabels()
        {
            try
            {
                if (Manager.Instance.ApplicationData.WorkLabels.Count > 0)
                    CurrentUserControl = new WorkLabelView(this);
                else
                    await Messages.MsgWarning("Pracovní štítky doposud nebyly vygenerovány. Přidejte předměty, studijní skupiny a poté studijním skupinám " +
                        "přiřaďte předměty. Poté se pracovní štítky automaticky vygenerují");
            }
            catch (Exception ex)
            {
                log.Error("Go uc work labels failed!!!", ex);
                await Messages.MsgError($"Při zobrazování obrazovky pro zobrazení pracovních štítků nastala chyba - {ex.Message}");
            }
        }

        #endregion

        #region UserControls

        private UserControl _currentUserControl;
        public UserControl CurrentUserControl { get => _currentUserControl; set { _currentUserControl = value; OnPropertyChanged(nameof(CurrentUserControl)); } }

        private SubjectsView _ucSubjectsView;
        private EmployeesView _ucEmployess;
        private StudyGroupsView _ucStudyGroups;

        #endregion

        #region Methods

        public void ReloadUserControls()
        {
            _ucEmployess = new EmployeesView(this);
            _ucStudyGroups = new StudyGroupsView(this);
            _ucSubjectsView = new SubjectsView(this);
        }

        private void InitCommands()
        {
            CommandGoUcSubjects = new CommandGeneric(GoUcSubjects);
            CommandGoUcEmployees = new CommandGeneric(GoUcEmployees);
            CommandGoUcStudyGroups = new CommandGeneric(GoUcStudyGroups);
            CommandGoUcSettings = new CommandGeneric(GoUcSettings);
            CommandGoUcWeights = new CommandGeneric(GoUcWeights);
            CommandGoUcWorkLabels = new CommandGeneric(GoUcWorkLabels);
        }

        public void GenerateOrEditWorkLabels()
        {
            if (Manager.Instance.ApplicationData.WorkLabels.Count <= 0) TryGenerateWorkLabels();
            else TryGenerateWorkLabelsForNewAddedSubjects();
        }

        private void TryGenerateWorkLabels()
        {
            var studyGroups = Manager.Instance.ApplicationData.StudyGroups;
            if (studyGroups.Count <= 0) return;
            if (studyGroups.All(x => string.IsNullOrWhiteSpace(x.SubjectIds))) return;

            foreach(var item in studyGroups)
            {
                var subIds = item.SubjectIds.Split(',');
                foreach(var subjectId in subIds)
                {
                    var subId = int.Parse(subjectId);
                    var subject = Manager.Instance.ApplicationData.Subjects.FirstOrDefault(x => x.Id == subId);
                    if (subject == null) continue;

                    // vytvoření štítku přednášky
                    if (subject.LectureLength > 0) CreateWorkLabel(item, subject, LabelTypes.Lecture, subject.LectureLength, subject.WeekCount);
                    // vytvoření štítku zápočtu
                    if (subject.FinishType == FinishTypes.Credit || subject.FinishType == FinishTypes.Exam)
                        CreateWorkLabel(item, subject, LabelTypes.Credit, 0, 0);
                    // vytvoření štítku klasifikovaný zápočet
                    if (subject.FinishType == FinishTypes.ClassifiedCredit) CreateWorkLabel(item, subject, LabelTypes.ClassifiedCredit, 0, 0);
                    // vytvoření štítku zkouška
                    if (subject.FinishType == FinishTypes.Exam) CreateWorkLabel(item, subject, LabelTypes.Exam, 0, 0);

                    // vytvoření štítků pro cvičení
                    if (subject.PracticeLength > 0) CreateWorkLabel(item, subject, LabelTypes.Practice, subject.PracticeLength, subject.WeekCount);
                    // vytvoření štítků pro semináře
                    if (subject.SeminarLength > 0) CreateWorkLabel(item, subject, LabelTypes.Seminar, subject.SeminarLength, subject.WeekCount);
                }
            }
            Manager.Instance.SerializeAppData();
        }

        private void TryGenerateWorkLabelsForNewAddedSubjects()
        {
            var studyGroups = Manager.Instance.ApplicationData.StudyGroups;
            if (studyGroups.Count <= 0) return;
            if (studyGroups.All(x => string.IsNullOrWhiteSpace(x.SubjectIds))) return;

            foreach(var studyGroup in studyGroups)
            {
                var subIds = studyGroup.SubjectIds?.Split(',');
                if (subIds == null || subIds?.Length <= 0) continue;
                foreach (var subjectId in subIds)
                {
                    var subId = int.Parse(subjectId);
                    var wl = Manager.Instance.ApplicationData.WorkLabels.Where(x => x.SubjectId == subId && x.StudyGroupId == studyGroup.Id).ToList();
                    if (wl != null && wl.Count > 0) continue;
                    // else generate work labels for new assigned subjects to study groups

                    var subject = Manager.Instance.ApplicationData.Subjects.FirstOrDefault(x => x.Id == subId);
                    if (subject == null) continue;

                    // vytvoření štítku přednášky
                    if (subject.LectureLength > 0) CreateWorkLabel(studyGroup, subject, LabelTypes.Lecture, subject.SeminarLength, subject.WeekCount);
                    // vytvoření štítku zápočtu
                    if (subject.FinishType == FinishTypes.Credit || subject.FinishType == FinishTypes.Exam)
                        CreateWorkLabel(studyGroup, subject, LabelTypes.Credit, 0, 0);
                    // vytvoření štítku klasifikovaný zápočet
                    if (subject.FinishType == FinishTypes.ClassifiedCredit) CreateWorkLabel(studyGroup, subject, LabelTypes.ClassifiedCredit, 0, 0);
                    // vytvoření štítku zkouška
                    if (subject.FinishType == FinishTypes.Exam) CreateWorkLabel(studyGroup, subject, LabelTypes.Exam, 0, 0);

                    // vytvoření štítků pro cvičení
                    if (subject.PracticeLength > 0) CreateWorkLabel(studyGroup, subject, LabelTypes.Practice, subject.PracticeLength, subject.WeekCount);
                    // vytvoření štítků pro semináře
                    if (subject.SeminarLength > 0) CreateWorkLabel(studyGroup, subject, LabelTypes.Seminar, subject.SeminarLength, subject.WeekCount);
                }
            }
            Manager.Instance.SerializeAppData();
        }

        private void CreateWorkLabel(StudyGroup studyGroup, Subject subject, LabelTypes labelType, int hoursLength, int weekCount)
        {
            if (labelType == LabelTypes.Practice || labelType == LabelTypes.Seminar)
            {
                var labelsToCreate = Math.Ceiling(studyGroup.StudentCount / (decimal)subject.StudentCount);
                var studentCountForLabel = Math.Floor(studyGroup.StudentCount / labelsToCreate);
                var studentsCount = new List<decimal>();
                for (int i = 1; i <= labelsToCreate; i++) studentsCount.Add(studentCountForLabel);
                for (var i = labelsToCreate * studentCountForLabel; i < studyGroup.StudentCount; i++)
                    studentsCount[(int)(i % labelsToCreate)]++;

                var cnt = 1;
                foreach(var item in studentsCount)
                {
                    var label = new WorkLabel
                    {
                        Id = Manager.Instance.Settings.LastWorkLabelId,
                        LabelType = labelType,
                        Language = subject.Language,
                        StudentCount = (int)item,
                        StudyGroupId = studyGroup.Id,
                        WeekCount = weekCount,
                        SubjectId = subject.Id,
                        Name = $"{EnumExtensions.GetDisplayName(labelType)} {subject.Shortcut} {cnt}",
                        HoursLength = hoursLength,
                    };
                    Manager.Instance.IncrementWorkLabelId();
                    Manager.Instance.ApplicationData.WorkLabels.Add(label);
                    cnt++;
                }
            }
            else
            {
                var label = new WorkLabel
                {
                    Id = Manager.Instance.Settings.LastWorkLabelId,
                    LabelType = labelType,
                    Language = subject.Language,
                    StudentCount = studyGroup.StudentCount,
                    StudyGroupId = studyGroup.Id,
                    WeekCount = weekCount,
                    SubjectId = subject.Id,
                    Name = $"{EnumExtensions.GetDisplayName(labelType)} {subject.Shortcut}",
                    HoursLength = hoursLength,
                };
                Manager.Instance.IncrementWorkLabelId();
                Manager.Instance.ApplicationData.WorkLabels.Add(label);
            }
        }

        public void UnsetWorkLabelsForDeletedEmployees(List<int> deletedEmployeesIds)
        {
            if(deletedEmployeesIds != null && deletedEmployeesIds.Count > 0)
            {
                foreach(var id in deletedEmployeesIds)
                {
                    var labels = Manager.Instance.ApplicationData.WorkLabels.Where(x => x.EmployeeId == id).ToList();
                    if(labels != null && labels.Count > 0)
                    {
                        foreach (var label in labels) label.EmployeeId = null;
                    }
                }
            }
        }

        #endregion

    }
}
