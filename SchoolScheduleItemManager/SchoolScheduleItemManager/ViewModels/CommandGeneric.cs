﻿using System.Windows.Input;

namespace SchoolScheduleItemManager.ViewModels
{
    public delegate void ToExecute();
    public delegate void ToExecuteWithParam(object param);
    public delegate bool CanExecute(object param);

    class CommandGeneric : ICommand
    {
        private readonly ToExecute _toExecute;
        private readonly ToExecuteWithParam _toExecuteWithParam;
        private readonly CanExecute _canExecute;

        public CommandGeneric(ToExecute toExecute, CanExecute canExecute = null)
        {
            _toExecute = toExecute;
            _canExecute = canExecute;
        }

        public CommandGeneric(ToExecuteWithParam toExecuteWithParam, CanExecute canExecute = null)
        {
            _toExecuteWithParam = toExecuteWithParam;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter) => _canExecute == null || _canExecute(parameter);

        public event System.EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        public void Execute(object parameter)
        {
            _toExecute?.Invoke();

            _toExecuteWithParam?.Invoke(parameter);
        }
    }
}
