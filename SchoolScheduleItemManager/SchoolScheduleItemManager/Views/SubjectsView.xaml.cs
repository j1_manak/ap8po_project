﻿using SchoolScheduleItemManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SchoolScheduleItemManager.Views
{
    /// <summary>
    /// Interaction logic for SubjectsView.xaml
    /// </summary>
    public partial class SubjectsView : UserControl
    {
        private readonly SubjectsVM _vmSubjects;
        public SubjectsView(MainWinVM vmMainWin)
        {
            _vmSubjects = new SubjectsVM(vmMainWin);
            DataContext = _vmSubjects;
            InitializeComponent();
        }
    }
}
