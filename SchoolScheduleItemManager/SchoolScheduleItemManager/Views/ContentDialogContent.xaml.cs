﻿using SchoolScheduleItemManager.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SchoolScheduleItemManager.Views
{
    /// <summary>
    /// Interaction logic for ContentDialogContent.xaml
    /// </summary>
    public partial class ContentDialogContent : UserControl
    {
        public ContentDialogContent(Icons icon, string text)
        {
            InitializeComponent();
            tbDialogText.Text = text;
            SetIcon(icon);
        }

        private void SetIcon(Icons icon)
        {
            var imgPath = "";
            switch (icon)
            {
                case Icons.Error: imgPath = @"..\..\Images\cancel.png"; break;
                case Icons.Info: imgPath = @"..\..\Images\info.png"; break;
                case Icons.Warning: imgPath = @"..\..\Images\warning.png"; break;
                default: imgPath = @"..\..\Images\question.png"; break;
            }
            imgIcon.Source = new BitmapImage(new Uri(imgPath, UriKind.Relative));
        }
    }
}
