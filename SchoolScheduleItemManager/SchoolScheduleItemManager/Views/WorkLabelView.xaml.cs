﻿using SchoolScheduleItemManager.ViewModels;
using System.Windows.Controls;

namespace SchoolScheduleItemManager.Views
{
    public partial class WorkLabelView : UserControl
    {
        private readonly WorkLabelVM _vmWorkLabels;
        public WorkLabelView(MainWinVM vmMainWin)
        {
            _vmWorkLabels = new WorkLabelVM(vmMainWin);
            DataContext = _vmWorkLabels;
            InitializeComponent();
        }
    }
}