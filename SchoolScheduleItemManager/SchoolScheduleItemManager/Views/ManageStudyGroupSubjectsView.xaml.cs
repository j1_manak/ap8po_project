﻿using ModernWpf.Controls;
using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SchoolScheduleItemManager.Views
{
    /// <summary>
    /// Interaction logic for ManageStudyGroupSubjectsView.xaml
    /// </summary>
    public partial class ManageStudyGroupSubjectsView : UserControl
    {
        private readonly ManageStudyGroupSubjectsVM _vmManageStudyGroupSubjects;

        public ManageStudyGroupSubjectsView(ContentDialog cd, StudyGroup selectedStudyGroup)
        {
            _vmManageStudyGroupSubjects = new ManageStudyGroupSubjectsVM(cd, selectedStudyGroup);
            DataContext = _vmManageStudyGroupSubjects;
            InitializeComponent();
        }
    }
}
