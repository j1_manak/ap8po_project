﻿using SchoolScheduleItemManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SchoolScheduleItemManager.Views
{
    /// <summary>
    /// Interaction logic for EmployeesView.xaml
    /// </summary>
    public partial class EmployeesView : UserControl
    {
        private readonly EmployeesVM _vmEmployees;
        public EmployeesView(MainWinVM vmMainWin)
        {
            _vmEmployees = new EmployeesVM(vmMainWin);
            DataContext = _vmEmployees;
            InitializeComponent();
        }
    }
}
