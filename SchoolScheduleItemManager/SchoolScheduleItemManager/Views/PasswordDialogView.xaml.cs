﻿using ModernWpf.Controls;
using SchoolScheduleItemManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SchoolScheduleItemManager.Views
{
    /// <summary>
    /// Interaction logic for PasswordDialogView.xaml
    /// </summary>
    public partial class PasswordDialogView : UserControl
    {
        private readonly PasswordDialogVM _vmPasswordDialog;
        public PasswordDialogView(ContentDialog cd, string emailAddress)
        {
            _vmPasswordDialog = new PasswordDialogVM(cd, emailAddress, this);
            DataContext = _vmPasswordDialog;
            InitializeComponent();
        }

        public bool DialogRes { get; set; } = false;
        public string EmailPassword => _vmPasswordDialog?.EmailAddressText;

    }
}
