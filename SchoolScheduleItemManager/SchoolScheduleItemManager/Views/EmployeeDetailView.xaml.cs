﻿using SchoolScheduleItemManager.Models;
using SchoolScheduleItemManager.ViewModels;
using System.Windows.Controls;

namespace SchoolScheduleItemManager.Views
{
    /// <summary>
    /// Interaction logic for EmployeeDetailView.xaml
    /// </summary>
    public partial class EmployeeDetailView : UserControl
    {
        private readonly EmployeeDetailVM _vmEmployeeDetail;
        public EmployeeDetailView(MainWinVM vmMainWin, Employee selectedEmployee)
        {
            _vmEmployeeDetail = new EmployeeDetailVM(vmMainWin, selectedEmployee);
            DataContext = _vmEmployeeDetail;
            InitializeComponent();
        }
    }
}
