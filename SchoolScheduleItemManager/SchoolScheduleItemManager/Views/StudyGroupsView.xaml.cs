﻿using SchoolScheduleItemManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SchoolScheduleItemManager.Views
{
    /// <summary>
    /// Interaction logic for StudyGroupsView.xaml
    /// </summary>
    public partial class StudyGroupsView : UserControl
    {
        private readonly StudyGroupsVM _vmStudyGroups;
        public StudyGroupsView(MainWinVM vmMainWin)
        {
            _vmStudyGroups = new StudyGroupsVM(vmMainWin);
            DataContext = _vmStudyGroups;
            InitializeComponent();
        }
    }
}
