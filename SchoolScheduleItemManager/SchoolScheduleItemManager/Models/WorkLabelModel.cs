﻿using SchoolScheduleItemManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolScheduleItemManager.Models
{
    public class WorkLabelModel : BaseVM
    {
        public int Id { get; set; }
        public string LabelName { get; set; }
        private Employee _employee;
        public Employee Employee { get => _employee; set { _employee = value; OnPropertyChanged(nameof(_employee)); } }
        public Subject Subject { get; set; }
        public StudyGroup StudyGroup { get; set; }
        public LabelTypes LabelType { get; set; }
        public int StudentCount { get; set; }
        public int WeekCount { get; set; }
        public int HoursLength { get; set; }
        public Languages Language { get; set; }
    }
}
