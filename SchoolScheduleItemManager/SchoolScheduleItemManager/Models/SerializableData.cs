﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolScheduleItemManager.Models
{
    public class SerializableData
    {
        public SerializableData()
        {
            Subjects = new ObservableCollection<Subject>();
            Employees = new ObservableCollection<Employee>();
            StudyGroups = new ObservableCollection<StudyGroup>();
            WorkLabels = new ObservableCollection<WorkLabel>();
        }

        public ObservableCollection<Subject> Subjects { get; set; }
        public ObservableCollection<Employee> Employees { get; set; }
        public ObservableCollection<StudyGroup> StudyGroups { get; set; }
        public ObservableCollection<WorkLabel> WorkLabels { get; set; }
    }
}
