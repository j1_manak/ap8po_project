﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SchoolScheduleItemManager.Models
{
    public enum ContractTypes 
    {
        [Display(Name = "Pracovní úvazek")]
        Full, 
        [Display(Name = "Doktorand")]
        PhdStudent 
    }

    public class Employee
    {
        public Employee()
        { 

        }

        public Employee(Employee val)
        {
            Id = val.Id;
            FirstName = string.Copy(val.FirstName);
            LastName = string.Copy(val.LastName);
            WorkEmail = string.Copy(val.WorkEmail);
            PrivateEmail = string.Copy(val.PrivateEmail);
            Contract = val.Contract;
            ContractSize = val.ContractSize;
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Fullname => $"{FirstName} {LastName}";
        public string WorkEmail { get; set; }
        public string PrivateEmail { get; set; }
        public ContractTypes Contract { get; set; }
        public string ContractSize { get; set; }

        [XmlIgnore]
        public double WorkingHoursNoEnglish
        {
            get
            {
                if(Manager.Instance?.ApplicationData.WorkLabels != null && Manager.Instance.ApplicationData.WorkLabels.Count > 0)
                {
                    var labels = Manager.Instance.ApplicationData.WorkLabels.Where(x => x.EmployeeId == Id).ToList();
                    if(labels != null && labels.Count > 0)
                    {
                        var pointValues = Manager.Instance.Points;
                        double points = 0;
                        foreach(var label in labels)
                        {
                            double val = 0;
                            switch (label.LabelType)
                            {
                                case LabelTypes.ClassifiedCredit:
                                    if (label.Language == Languages.Czech) val = double.Parse(pointValues.GiveOneClassifiedCredit);
                                    break;
                                case LabelTypes.Credit:
                                    if (label.Language == Languages.Czech) val = double.Parse(pointValues.GiveOneCredit);
                                    break;
                                case LabelTypes.Exam:
                                    if (label.Language == Languages.Czech) val = double.Parse(pointValues.GiveOneExam);
                                    break;
                                case LabelTypes.Lecture:
                                    if (label.Language == Languages.Czech) val = double.Parse(pointValues.Lecture);
                                    break;
                                case LabelTypes.Practice:
                                    if (label.Language == Languages.Czech) val = double.Parse(pointValues.Practice);
                                    break;
                                case LabelTypes.Seminar:
                                    if (label.Language == Languages.Czech) val = double.Parse(pointValues.Seminar);
                                    break;
                            }
                            if (label.LabelType != LabelTypes.ClassifiedCredit && label.LabelType != LabelTypes.Credit && label.LabelType != LabelTypes.Exam)
                                points += val * label.HoursLength * label.WeekCount;
                            else
                                points += val;
                        }
                        return points;
                    }
                    return 0;
                }
                return 0;
            }
        }

        [XmlIgnore]
        public double WorkingHoursWithEnglish
        {
            get
            {
                if (Manager.Instance?.ApplicationData.WorkLabels != null && Manager.Instance.ApplicationData.WorkLabels.Count > 0)
                {
                    var labels = Manager.Instance.ApplicationData.WorkLabels.Where(x => x.EmployeeId == Id).ToList();
                    if (labels != null && labels.Count > 0)
                    {
                        var pointValues = Manager.Instance.Points;
                        double points = 0;
                        foreach (var label in labels)
                        {
                            double val = 0;
                            switch (label.LabelType)
                            {
                                case LabelTypes.ClassifiedCredit:
                                    if (label.Language == Languages.Czech) val = double.Parse(pointValues.GiveOneClassifiedCredit);
                                    else val = double.Parse(pointValues.GiveOneClassifiedCreditEnglish);
                                    break;
                                case LabelTypes.Credit:
                                    if (label.Language == Languages.Czech) val = double.Parse(pointValues.GiveOneCredit);
                                    else val = double.Parse(pointValues.GiveOneCreditEnglish);
                                    break;
                                case LabelTypes.Exam:
                                    if (label.Language == Languages.Czech) val = double.Parse(pointValues.GiveOneExam);
                                    else val = double.Parse(pointValues.GiveOneExamEnglish);
                                    break;
                                case LabelTypes.Lecture:
                                    if (label.Language == Languages.Czech) val = double.Parse(pointValues.Lecture);
                                    else val = double.Parse(pointValues.LectureEnglish);
                                    break;
                                case LabelTypes.Practice:
                                    if (label.Language == Languages.Czech) val = double.Parse(pointValues.Practice);
                                    else val = double.Parse(pointValues.PracticeEnglish);
                                    break;
                                case LabelTypes.Seminar:
                                    if (label.Language == Languages.Czech) val = double.Parse(pointValues.Seminar);
                                    else val = double.Parse(pointValues.SeminarEnglish);
                                    break;
                            }
                            if (label.LabelType != LabelTypes.ClassifiedCredit && label.LabelType != LabelTypes.Credit && label.LabelType != LabelTypes.Exam)
                                points += val * label.HoursLength * label.WeekCount;
                            else
                                points += val;
                        }
                        return points;
                    }
                    return 0;
                }
                return 0;
            }
        }

        public override string ToString() => $"{FirstName} {LastName}";
    }
}
