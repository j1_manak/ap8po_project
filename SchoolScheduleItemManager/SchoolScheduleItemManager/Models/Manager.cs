﻿using Newtonsoft.Json;
using SchoolScheduleItemManager.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace SchoolScheduleItemManager.Models
{
    public class Manager
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string AppDataXmlFilePath = $"{SystemUtils.UserLocalAppDirectory}AppData.xml";
        private string AppDataDbFilePath = $"{SystemUtils.UserLocalAppDirectory}AppData.mdf";
        private string SettingsFilePath = $"{SystemUtils.UserLocalAppDirectory}Settings.xml";
        private string PointsFilePath = $"{SystemUtils.UserLocalAppDirectory}Points.xml";

        public Manager()
        {
            DeserializeSettings();
            DeserializeData();
            DeserializePoints();
        }

        #region Singleton

        private static volatile Manager _instance;

        public static Manager Instance
        {
            get
            {
                if (_instance == null) _instance = new Manager();

                return _instance;
            }
        }

        #endregion

        #region Properties

        public AppDbContext AppDb { get; set; }
        public SerializableData ApplicationData { get; set; }
        public SettingsModel Settings { get; set; }
        public PointWeightsModel Points { get; set; }

        #endregion

        #region Methods

        public void SerializeAppData()
        {
            try
            {
                if (Settings.SelectedStorageType == StorageTypes.Xml)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SerializableData));
                    using (var fs = new FileStream(AppDataXmlFilePath, FileMode.Create))
                    {
                        serializer.Serialize(fs, ApplicationData);
                    }
                }
                else // database
                {
                    AppDb.StudyGroups.AddOrUpdate(ApplicationData.StudyGroups.ToArray());
                    foreach(var sg in AppDb.StudyGroups)
                    {
                        if (!ApplicationData.StudyGroups.Any(x => x.Id == sg.Id))
                            AppDb.StudyGroups.Remove(sg);
                    }
                    AppDb.Subjects.AddOrUpdate(ApplicationData.Subjects.ToArray());
                    foreach (var subject in AppDb.Subjects)
                    {
                        if (!ApplicationData.Subjects.Any(x => x.Id == subject.Id))
                            AppDb.Subjects.Remove(subject);
                    }
                    AppDb.Employees.AddOrUpdate(ApplicationData.Employees.ToArray());
                    foreach (var employee in AppDb.Employees)
                    {
                        if (!ApplicationData.Employees.Any(x => x.Id == employee.Id))
                            AppDb.Employees.Remove(employee);
                    }
                    AppDb.WorkLabels.AddOrUpdate(ApplicationData.WorkLabels.ToArray());
                    foreach(var wl in AppDb.WorkLabels)
                    {
                        if (!ApplicationData.WorkLabels.Any(x => x.Id == wl.Id))
                            AppDb.WorkLabels.Remove(wl);
                    }
                    AppDb.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                log.Error("Serialize subjects failed!!!", ex);
            }
        }

        private void DeserializeData()
        {
            try
            {
                if (Settings.SelectedStorageType == StorageTypes.Xml)
                {
                    if (File.Exists(AppDataXmlFilePath))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(SerializableData));
                        using (var fs = new FileStream(AppDataXmlFilePath, FileMode.Open))
                        {
                            ApplicationData = (SerializableData)serializer.Deserialize(fs);
                        }
                    }
                    else ApplicationData = new SerializableData();
                }
                else // database
                {
                    AppDb = new AppDbContext($"Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename={AppDataDbFilePath};Integrated Security=True;Connect Timeout=30");
                    AppDb.Database.CreateIfNotExists();
                    ApplicationData = new SerializableData
                    {
                        Employees = SystemUtils.ConvertListToOC(AppDb.Employees.ToList()),
                        StudyGroups = SystemUtils.ConvertListToOC(AppDb.StudyGroups.ToList()),
                        Subjects = SystemUtils.ConvertListToOC(AppDb.Subjects.ToList()),
                        WorkLabels = SystemUtils.ConvertListToOC(AppDb.WorkLabels.ToList())
                    };
                }
            }
            catch (Exception ex)
            {
                log.Error("Deserilize subjects failed!!!", ex);
                ApplicationData = new SerializableData();
            }
        }

        public void SerializeSettings()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SettingsModel));
                using(var fs = new FileStream(SettingsFilePath, FileMode.Create))
                {
                    serializer.Serialize(fs, Settings);
                }
            }
            catch (Exception ex)
            {
                log.Error("Serialize settings failed!!!", ex);
            }
        }

        private void DeserializeSettings()
        {
            try
            {
                if (File.Exists(SettingsFilePath))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SettingsModel));
                    using (var fs = new FileStream(SettingsFilePath, FileMode.Open))
                    {
                        Settings = (SettingsModel)serializer.Deserialize(fs);
                    }
                }
                else Settings = new SettingsModel();
            }
            catch (Exception ex)
            {
                log.Error("Deserialize settings failed!!!", ex);
                Settings = new SettingsModel();
            }
        }

        /*public void ImportAppData(string appDataFilePath)
        {
            var fi = new FileInfo(appDataFilePath);
            if(fi.Extension == ".xml") File.Copy(appDataFilePath, AppDataXmlFilePath);
            if (fi.Extension == ".mdf") File.Copy(appDataFilePath, AppDataDbFilePath);
            DeserializeData();
        }*/

        /*public void ExportAppData(StorageTypes storageType, string exportPath)
        {
            if(storageType == StorageTypes.Xml)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SerializableData));
                using (var fs = new FileStream(exportPath, FileMode.Create))
                {
                    serializer.Serialize(fs, ApplicationData);
                }
            }
            else //db - TODO: implement me
            {
                AppDb.Dispose();
                var fi = new FileInfo(AppDataDbFilePath);
                File.Copy(AppDataDbFilePath, exportPath);
                AppDb = new AppDbContext($"Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename={AppDataDbFilePath};Integrated Security=True;Connect Timeout=30");
            }
        }*/

        public void SerializePoints()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(PointWeightsModel));
                using (var fs = new FileStream(PointsFilePath, FileMode.Create))
                {
                    serializer.Serialize(fs, Points);
                }
            }
            catch (Exception ex)
            {
                log.Error("Serialize points failed!!!", ex);
            }
        }

        private void DeserializePoints()
        {
            try
            {
                if (File.Exists(PointsFilePath))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(PointWeightsModel));
                    using (var fs = new FileStream(PointsFilePath, FileMode.Open))
                    {
                        Points = (PointWeightsModel)serializer.Deserialize(fs);
                    }
                }
                else Points = new PointWeightsModel();
            }
            catch (Exception ex)
            {
                log.Error("Deserialize points failed!!!", ex);
            }
        }

        public void IncrementSubjetId()
        {
            Settings.LastSubjectId++;
            SerializeSettings();
        }

        public void IncrementWorkLabelId()
        {
            Settings.LastWorkLabelId++;
            SerializeSettings();
        }

        public void IncrementEmployeeId()
        {
            Settings.LastEmployeeId++;
            SerializeSettings();
        }

        public void IncrementStudyGroupId()
        {
            Settings.LastStudyGroupId++;
            SerializeSettings();
        }

        public void SwitchDataStorage()
        {
            try
            {
                DeserializeData();
                Settings.LastEmployeeId = ApplicationData.Employees.Count > 0 ? ApplicationData.Employees.Last().Id + 1 : 0;
                Settings.LastStudyGroupId = ApplicationData.StudyGroups.Count > 0 ? ApplicationData.StudyGroups.Last().Id + 1 : 0;
                Settings.LastSubjectId = ApplicationData.Subjects.Count > 0 ? ApplicationData.Subjects.Last().Id + 1 : 0;
                SerializeSettings();
            }
            catch (Exception ex)
            {
                log.Error("Swithching data storage failed!!!", ex);
            }
        }

        #endregion
    }
}
