﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolScheduleItemManager.Models
{
    public class PointWeightsModel
    {
        public PointWeightsModel()
        {
            Lecture = "1.8";
            Practice = "1.2";
            Seminar = "1.2";
            LectureEnglish = "2.4";
            PracticeEnglish = "1.8";
            SeminarEnglish = "1.8";
            GiveOneCredit = "0.2";
            GiveOneClassifiedCredit = "0.3";
            GiveOneExam = "0.4";
            GiveOneCreditEnglish = "0.2";
            GiveOneClassifiedCreditEnglish = "0.2";
            GiveOneExamEnglish = "0.4";
        }

        public string Lecture { get; set; }
        public string Practice { get; set; }
        public string Seminar { get; set; }
        public string LectureEnglish { get; set; }
        public string PracticeEnglish { get; set; }
        public string SeminarEnglish { get; set; }
        public string GiveOneCredit { get; set; }
        public string GiveOneClassifiedCredit { get; set; }
        public string GiveOneExam { get; set; }
        public string GiveOneCreditEnglish { get; set; }
        public string GiveOneClassifiedCreditEnglish { get; set; }
        public string GiveOneExamEnglish { get; set; }

        public bool IsValid()
        {
            var res = false;
            res = double.TryParse(Lecture, out _);
            res = double.TryParse(Practice, out _);
            res = double.TryParse(Seminar, out _);
            res = double.TryParse(LectureEnglish, out _);
            res = double.TryParse(PracticeEnglish, out _);
            res = double.TryParse(SeminarEnglish, out _);
            res = double.TryParse(GiveOneCredit, out _);
            res = double.TryParse(GiveOneClassifiedCredit, out _);
            res = double.TryParse(GiveOneExam, out _);
            res = double.TryParse(GiveOneCreditEnglish, out _);
            res = double.TryParse(GiveOneClassifiedCreditEnglish, out _);
            res = double.TryParse(GiveOneExamEnglish, out _);
            return res;
        }
    }
}
