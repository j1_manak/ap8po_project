﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolScheduleItemManager.Models
{
    public enum StorageTypes { Xml, Database}
    public class SettingsModel
    {
        public SettingsModel()
        {
            SelectedStorageType = StorageTypes.Xml;
        }

        public StorageTypes SelectedStorageType { get; set; }

        public string SmtpServerAddress { get; set; }
        public int SmtpServerPort { get; set; }
        public string EmailAddress { get; set; }

        public int LastSubjectId { get; set; }
        public int LastStudyGroupId { get; set; }
        public int LastEmployeeId { get; set; }
        public int LastWorkLabelId { get; set; }
    }
}
