﻿using SchoolScheduleItemManager.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolScheduleItemManager.Models
{
    public enum Semesters 
    {
        [Display(Name = "Zimní")]
        Winter, 
        [Display(Name = "Letní")]
        Summer
    }
    public enum StudyForms 
    {
        [Display(Name = "Prezenční")]
        Present, 
        [Display(Name = "Kombinovaná")]
        Combine 
    }
    public enum StudyTypes 
    {
        [Display(Name = "Bakalářský")]
        Bc, 
        [Display(Name = "Magisterský")]
        Ing 
    }

    public class StudyGroup
    {
        public StudyGroup()
        {
            Semester = Semesters.Winter;
            StudyForm = StudyForms.Present;
            StudyType = StudyTypes.Bc;
            Language = Languages.Czech;
        }

        public StudyGroup(StudyGroup original)
        {
            Id = original.Id;
            Shortcut = string.Copy(original.Shortcut);
            StudyYear = original.StudyYear;
            Semester = original.Semester;
            StudentCount = original.StudentCount;
            StudyForm = original.StudyForm;
            StudyType = original.StudyType;
            Language = original.Language;
            SubjectIds = original.SubjectIds;
        }

        public int Id { get; set; }
        public string Shortcut { get; set; }
        public int StudyYear { get; set; }
        public Semesters? Semester { get; set; }
        public int StudentCount { get; set; }
        public StudyForms? StudyForm { get; set; }
        public StudyTypes? StudyType { get; set; }
        public Languages? Language { get; set; }
        public string SubjectIds { get; set; }

        public void AddSubject(string subjectId)
        {
            if (string.IsNullOrWhiteSpace(SubjectIds)) SubjectIds = subjectId;
            else SubjectIds += $",{subjectId}";
        }

        public void RemoveSubject(string subjectId)
        {
            if (string.IsNullOrWhiteSpace(SubjectIds)) return;

            if (SubjectIds.Contains($",{subjectId}")) SubjectIds = SubjectIds.Replace($",{subjectId}", string.Empty);
            else if (SubjectIds.Contains(subjectId)) SubjectIds = SubjectIds.Replace($"{subjectId}", string.Empty);
        }

        public override string ToString() => $"{Shortcut} {StudyYear} {EnumExtensions.GetDisplayName(Semester)}";
    }
}
