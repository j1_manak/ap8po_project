﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolScheduleItemManager.Models
{
    public enum LabelTypes 
    {
        [Display(Name = "Přednáška")]
        Lecture, 
        [Display(Name = "Seminář")]
        Seminar, 
        [Display(Name = "Cvičení")]
        Practice, 
        [Display(Name = "Zápočet")]
        Credit, 
        [Display(Name = "Klasifikovaný zápočet")]
        ClassifiedCredit, 
        [Display(Name = "Zkouška")]
        Exam 
    }
    public class WorkLabel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? EmployeeId { get; set; }
        public int? SubjectId { get; set; }
        public int? StudyGroupId { get; set; }
        public LabelTypes LabelType { get; set; }
        public int StudentCount { get; set; }
        public int WeekCount { get; set; }
        public int HoursLength { get; set; }
        public Languages Language { get; set; }
    }
}
