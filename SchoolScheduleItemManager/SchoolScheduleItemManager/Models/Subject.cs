﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolScheduleItemManager.Models
{
    public enum Languages 
    {
        [Display(Name = "Český")]
        Czech, 
        [Display(Name = "Anglický")]
        English 
    }
    public enum FinishTypes 
    {
        [Display(Name = "Zápočet")]
        Credit, 
        [Display(Name = "Klasifikovaný zápočet")]
        ClassifiedCredit, 
        [Display(Name = "Zkouška")]
        Exam 
    }

    public class Subject
    {
        public Subject()
        {
            WeekCount = 14;
            StudentCount = 24;
        }

        public Subject(Subject val)
        {
            Id = val.Id;
            Name = string.Copy(val.Name);
            Shortcut = string.Copy(val.Shortcut);
            WeekCount = val.WeekCount;
            LectureLength = val.LectureLength;
            SeminarLength = val.SeminarLength;
            PracticeLength = val.PracticeLength;
            Language = val.Language;
            StudentCount = val.StudentCount;
            FinishType = val.FinishType;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Shortcut { get; set; }
        public int WeekCount { get; set; }
        public int LectureLength { get; set; }
        public int SeminarLength { get; set; }
        public int PracticeLength { get; set; }
        public Languages Language { get; set; }
        public int StudentCount { get; set; }
        public FinishTypes FinishType { get; set; }
    }
}
