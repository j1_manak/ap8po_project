### Entities

1. Subject - Name (string), SubjectShortcut (string), WeekCount (short), LectureLength (short), SeminarLength(short), PracticeLength (short), Language (enum), StudentCount (short), FinishType (enum)
2. StudyGroup - Subjects (List of Subject), StudyField (string), YearOfStudy (short), SemestrType (enum), StudentCount (int), StudyForm (enum), StudyType (enum), Language (enum)
3. Employee - Title (string), FirstName (string), LastName (string), Obligation (enum - Full, Half, Deal), Email (string), PhoneNumber (string)
4. SubjectItem - Subject (Subject), Employee (Employee), Name (string), Type (enum), StudentCount (int), WeekCount (short), HoursCount(short), Language (enum),