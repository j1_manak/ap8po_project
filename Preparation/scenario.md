### Postup:
1. Vytvořit seznam předmětů - vytváření, editace, mazání
	- předměty musí mít svůj název, ústav a maximální kapacitu, která může být na přednášce / cvičení / semináři
2. Vkládání zaměstnanců - zaměstnance lze přidávat, editovat a mazat
	- Zaměstnanec bude mít jméno, ústav pod který patří a pracovní úvazek na fakultě (plný úvazek, poloviční, dohoda o pracovní činnosti)
3. Přidání skupinek studentů
	- skupinka bude mít obor, semestr, ročník počet studentů
4. Vygenerovat studijní události a přiřadit je zaměstnancům
	- zobrazíme všechny dostupné skupinky
	- každému tomuto oboru přiřadíme počet studentů
	- poté se počet studentů začne dělit kapacitou daného předmětu (záleží zda je to přednáška, cvičení)
	- vytvoří se události
	- zobrazí se tajemníkovi v tabulce
	- tajemník přiřazuje události manuálně zaměstnancům
	- vygenerované události se ukládají do databáze / xml

	
### Podmínky:
- data se budou moci ukládat ve dvou formátech - databáze a xml <br>
- váhy jednotlivých akcí (přednáška, cviko, lseminář) bude v xmlku a když jej někdo bude chtít z editovat, tak si upraví to xmlko (záleží i na jazyku ve kterém se učí)<br>
- 5 formulářů

### Otázky: 
